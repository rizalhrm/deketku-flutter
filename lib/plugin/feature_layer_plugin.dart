import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

import 'dart:convert';
import 'dart:async';

part 'feature_layer_plugin/feature_layer.dart';
part 'feature_layer_plugin/feature_layer_options.dart';
part 'feature_layer_plugin/polygon_esri.dart';
part 'feature_layer_plugin/feature_layer_controller.dart';
part 'feature_layer_plugin/feature_layer_event.dart';

class FeatureLayerPlugin extends MapPlugin {
  @override
  Widget createLayer(
      LayerOptions options, MapState mapState, Stream<void> stream) {
    return FeatureLayer(options, mapState, stream);
  }
  
  @override
  bool supportsLayer(LayerOptions options) {
    return options is FeatureLayerOptions;
  }
}