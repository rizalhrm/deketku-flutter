part of '../feature_layer_plugin.dart';

class FeatureLayerController {
  StreamController<FeatureLayerEvent> streamController;

  void resetPolygonGraphics() {
    streamController?.add(FeatureLayerEvent.resetPolygonGraphics());
  }

  void renderPolygonGraphics(String rings) {
    streamController?.add(FeatureLayerEvent.renderPolygonGraphics(rings));
  }
}
