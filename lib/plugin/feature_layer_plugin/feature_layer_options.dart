part of '../feature_layer_plugin.dart';

class AnimationsOptions {
  final Duration zoom;
  final Duration fitBound;
  final Curve fitBoundCurves;
  final Duration centerMarker;
  final Curve centerMarkerCurves;
  final Duration spiderfy;

  const AnimationsOptions({
    this.zoom = const Duration(milliseconds: 500),
    this.fitBound = const Duration(milliseconds: 500),
    this.centerMarker = const Duration(milliseconds: 500),
    this.spiderfy = const Duration(milliseconds: 500),
    this.fitBoundCurves = Curves.fastOutSlowIn,
    this.centerMarkerCurves = Curves.fastOutSlowIn,
  });
}

class FeatureLayerOptions extends LayerOptions {
  final Size size;

  /// A cluster will cover at most this many pixels from its center
  final int maxClusterRadius;

  final String rings;
  final FeatureLayerController featureLayerController;

  /// Options for fit bounds
  final FitBoundsOptions fitBoundsOptions;

  /// Zoom buonds with animation on click cluster
  final bool zoomToBoundsOnClick;

  /// animations options
  final AnimationsOptions animationsOptions;

  /// When click marker, center it with animation
  final bool centerMarkerOnClick;

  /// Increase to increase the distance away that circle spiderfied markers appear from the center
  final int spiderfyCircleRadius;

  /// Increase to increase the distance away that spiral spiderfied markers appear from the center
  final int spiderfySpiralDistanceMultiplier;

  /// Show spiral instead of circle from this marker count upwards.
  /// 0 -> always spiral; Infinity -> always circle
  final int circleSpiralSwitchover;
  /// Render
  final dynamic Function() render;

  /// Function to call when a Marker is tapped
  final void Function(dynamic attributes, LatLng location) onTap;
  
  FeatureLayerOptions({
    @required this.rings,
    rebuild,
    featureLayerController,
    this.size = const Size(30, 30),
    this.maxClusterRadius = 80,
    this.animationsOptions = const AnimationsOptions(),
    this.fitBoundsOptions =
        const FitBoundsOptions(padding: EdgeInsets.all(12.0)),
    this.zoomToBoundsOnClick = true,
    this.centerMarkerOnClick = true,
    this.spiderfyCircleRadius = 40,
    this.spiderfySpiralDistanceMultiplier = 1,
    this.circleSpiralSwitchover = 9,
    this.onTap,
    @required this.render
  }) : this.featureLayerController = featureLayerController ?? FeatureLayerController(),
        super(rebuild: rebuild);
}