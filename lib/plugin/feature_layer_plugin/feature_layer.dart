part of '../feature_layer_plugin.dart';

class FeatureLayer extends StatefulWidget {
  final FeatureLayerOptions options;
  final MapState map;
  final Stream<void> stream;

  FeatureLayer(this.options, this.map, this.stream);

  @override
  _FeatureLayerState createState() => _FeatureLayerState();
}

class _FeatureLayerState extends State<FeatureLayer> {
  @override
  initState() {
    super.initState();
    requestFeatures(widget.options.rings);
    widget.options.featureLayerController.streamController =
        StreamController<FeatureLayerEvent>.broadcast();
    widget.options.featureLayerController.streamController.stream
        .listen((FeatureLayerEvent featureLayerEvent) => _handleAction(featureLayerEvent));
  }

  void _handleAction(FeatureLayerEvent event) {
    switch (event.action) {
      case FeatureLayerEventActions.render:
        return requestFeatures(event.rings);
      case FeatureLayerEventActions.reset:
        return resetFeatures();
    }
  }

  List<dynamic> featuresPre = <dynamic>[];
  List<dynamic> features = <dynamic>[];

  void requestFeatures(String rings) async {
    List<dynamic> featuresL = [];
    var jsonData = json.decode(rings);
    for (var ring in jsonData) {
      var points = <LatLng>[];

      for (var point_ in ring) {
        points.add(LatLng(point_[1].toDouble(), point_[0].toDouble()));
      }

      var render = widget.options.render();

      featuresL.add(PolygonEsri(
        points: points,
        borderStrokeWidth: render.borderStrokeWidth,
        color: render.color,
        borderColor: render.borderColor,
        isDotted: render.isDotted,
      ));
    }
    setState(() {
      features = [...features, ...featuresL];
      featuresPre = [...featuresPre, ...featuresL];
    });
  }

  void resetFeatures() {
    setState(() {
      features = <dynamic>[];
      featuresPre = <dynamic>[];
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints bc) {
        final size = Size(bc.maxWidth, bc.maxHeight);
        return _buildPolygons(context, size);
      },
    );
  }

  Widget _buildPolygons(BuildContext context, Size size) {
    var elements = <Widget>[];
    if (features.isNotEmpty) {
      for (var polygon in features) {
        if (polygon is PolygonEsri) {
          polygon.offsets.clear();
          var i = 0;

          for (var point in polygon.points) {
            var pos = widget.map.project(point);
            pos = pos.multiplyBy(widget.map.getZoomScale(widget.map.zoom, widget.map.zoom)) - widget.map.getPixelOrigin();
            polygon.offsets.add(Offset(pos.x.toDouble(), pos.y.toDouble()));
            if (i > 0 && i < polygon.points.length) {
              polygon.offsets.add(Offset(pos.x.toDouble(), pos.y.toDouble()));
            }
            i++;
          }

          elements.add(
            CustomPaint(
              painter: PolygonPainter(polygon),
              size: size,
            )
          );
        }
      }
    }

    return Container(
      child: Stack(
        children: elements,
      ),
    );
  }

  @override
  void dispose() {
    widget.options.featureLayerController.streamController.close();
    super.dispose();
  }
}