part of '../feature_layer_plugin.dart';

class FeatureLayerEvent {
  final FeatureLayerEventActions action;
  String rings;

  FeatureLayerEvent.resetPolygonGraphics()
    : this.action = FeatureLayerEventActions.reset;
  
  FeatureLayerEvent.renderPolygonGraphics(this.rings)
    : this.action = FeatureLayerEventActions.render;
}

enum FeatureLayerEventActions {
  reset,
  render
}