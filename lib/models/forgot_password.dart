part of 'models.dart';

class ForgotPasswordData {
  String code;
  String email;
  String username;
  String tokenAdmin;
  int expires;

  ForgotPasswordData(
      {this.code = "",
      this.email = "",
      this.username = '',
      this.tokenAdmin = '',
      this.expires = 0});
}