part of 'models.dart';

class PolygonOptions extends Equatable {
  final Color color;
  final double borderStrokeWidth;
  final Color borderColor;
  final bool isDotted;

  PolygonOptions({
    this.color = const Color(0xFF00FF00),
    this.borderStrokeWidth = 0.0,
    this.borderColor = const Color(0xFFFFFF00),
    this.isDotted = false,
  });

  @override
  List<Object> get props => [color, borderStrokeWidth, borderColor, isDotted];
}

class Poi extends Equatable {
  final String attributes;
  final Marker marker;

  Poi(this.attributes, this.marker);

  @override
  List<Object> get props => [attributes, marker];
}