part of 'models.dart';

class Token extends Equatable {
  final int code;
  final String token;
  final int expires;
  final String message;
  final String details;
  final String username;
  final String password;

  Token({this.code, this.token, this.expires, this.message, this.details, this.username, this.password});

  factory Token.convertUser(Map<String, dynamic> data) {
    if (data['token'] != null) {
      return Token(
        code: 200,
        token: data['token'],
        expires: data['expires'],
      );
    }
    else {
      return Token(
        code: data['error']['code'],
        message: data['error']['message'],
        details: data['error']['details'],
      );
    }
  }

  factory Token.convertAdmin(Map<String, dynamic> data) {
    if (data['token'] != null) {
      return Token(
        code: 200,
        token: data['token'],
        expires: int.parse(data['expires'])
      );
    }
    else {
      return Token(
        code: data['code'],
        message: data['messages'][0]
      );
    }
  }

  @override
  List<Object> get props => [code, token, expires, message, details, username, password];
}