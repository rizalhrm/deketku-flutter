part of 'models.dart';

class Country {
  Country({
    this.name,
    this.code,
    this.id,
  });

  String name;
  String code;
  int id;

  factory Country.fromJson(Map<String, dynamic> json) => Country(
    name: json["name"],
    code: json["code"],
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "code": code,
    "id": id,
  };
}

class Gender {
  int id;
  String jenis;

  Gender(this.id,this.jenis);

  static List<Gender> getGenders() {
    return <Gender>[
      Gender(0, 'Laki-laki'),
      Gender(1, 'Perempuan')
    ];
  }
}

class CoordinatesInitial {
  double latitude;
  double longitude;

  CoordinatesInitial(
      {this.latitude= 0.3157754,
      this.longitude = 120.7839025});
}