part of 'models.dart';

class User extends Equatable {
  final int count;
  final Attributes attributes;
  final Error error;

  User({
    this.count,
    this.attributes,
    this.error
  });

  factory User.fromJson(Map<String, dynamic> data) {
    int count = data['features']?.length;
    if (count == null) {
      return User(
        error: Error.fromJson(data['error'])
      );
    }
    else {
      if (count > 0) {
        return User(
          count: count,
          attributes: Attributes.fromJson(data['features'][0])
        );
      }
      else {
        return User(
          count: 0
        );
      }
    }
  }

  @override
  List<Object> get props => [count, attributes];
}

class Attributes {
  final int objectid;
  final String userid;
  final String username;
  final String email;
  final int dob;
  final String alias;
  final int ismerchant;
  final int emailisverified;
  final int phoneisverified;
  final int usertier;
  final int userpoint;
  final int nationality;
  final int gender;
  final int uDate;
  final String favoritetag;
  final int defaultsearchby;
  final int defaultradiusvalue;
  final int defaultdrivingtimevalue;
  final int defaultdrivingdistance;
  final int userrole;
  final dynamic phonenumber;
  final String aaid;
  final String idfa;
  final int status;

  Attributes({
    this.objectid,
    this.userid,
    this.username,
    this.email,
    this.dob,
    this.alias,
    this.ismerchant,
    this.emailisverified,
    this.phoneisverified,
    this.usertier,
    this.userpoint,
    this.nationality,
    this.gender,
    this.uDate,
    this.favoritetag,
    this.defaultsearchby,
    this.defaultradiusvalue,
    this.defaultdrivingtimevalue,
    this.defaultdrivingdistance,
    this.userrole,
    this.phonenumber,
    this.aaid,
    this.idfa,
    this.status
  });

  factory Attributes.fromJson(Map<String, dynamic> data) {
    return Attributes(
      objectid: data['attributes']['objectid'],
      userid: data['attributes']['userid'],
      username: data['attributes']['username'],
      email: data['attributes']['email'],
      dob: data['attributes']['dob'],
      alias: data['attributes']['alias'],
      ismerchant: data['attributes']['ismerchant'],
      emailisverified: data['attributes']['emailisverified'],
      phoneisverified: data['attributes']['phoneisverified'],
      usertier: data['attributes']['usertier'],
      userpoint: data['attributes']['userpoint'],
      nationality: data['attributes']['nationality'],
      gender: data['attributes']['gender'],
      uDate: data['attributes']['u_date'],
      favoritetag: data['attributes']['favoritetag'],
      defaultsearchby: data['attributes']['defaultsearchby'],
      defaultradiusvalue: data['attributes']['defaultradiusvalue'],
      defaultdrivingtimevalue: data['attributes']['defaultdrivingtimevalue'],
      defaultdrivingdistance: data['attributes']['defaultdrivingdistance'],
      userrole: data['attributes']['userrole'],
      phonenumber: data['attributes']['phonenumber'],
      aaid: data['attributes']['aaid'],
      idfa: data['attributes']['idfa'],
      status: data['attributes']['status']
    );
  }
}

class UserCount extends Equatable {
  final int count;
  final Error error;

  UserCount({this.count, this.error});

  factory UserCount.fromJson(Map<String, dynamic> data) {
    int count = data['count'];
    if (count == null) {
      return UserCount(error: Error.fromJson(data['error']));
    }
    else {
      return UserCount(count: count);
    }
  }

  @override
  List<Object> get props => [count];
}

class Error {
  Error({
    this.code,
    this.message,
  });

  int code;
  String message;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
    code: json["code"],
    message: json["message"]
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "message": message
  };
}