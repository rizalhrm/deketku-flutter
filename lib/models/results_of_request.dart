part of 'models.dart';

class AddNewUser {
  AddNewUser({
    this.addResults,
    this.status,
  });

  String status;
  List<ResultNewUser> addResults;

  factory AddNewUser.fromJson(Map<String, dynamic> json, String result) {
    if (result.contains('success')) {
      return AddNewUser(
        status: "success",
        addResults: List<ResultNewUser>.from(json["addResults"].map((x) => ResultNewUser.fromJson(x))),
      ); 
    }
    else {
      return AddNewUser(
        status: "error"
      ); 
    }
  }

  Map<String, dynamic> toJson() => {
    "addResults": List<dynamic>.from(addResults.map((x) => x.toJson())),
  };
}

class ResultNewUser {
  ResultNewUser({
    this.objectId,
    this.success,
  });

  int objectId;
  bool success;

  factory ResultNewUser.fromJson(Map<String, dynamic> json) => ResultNewUser(
    objectId: json["objectId"],
    success: json["success"],
  );

  Map<String, dynamic> toJson() => {
    "objectId": objectId,
    "success": success,
  };
}