part of 'shared.dart';

const double defaultMargin1 = 40;
const double defaultMargin2 = 24;

Color primaryColor = Color(0xfff8ba0a);
Color accentColor1 = Color(0xfffbe307);
Color accentColor2 = Color(0xffffec50);
// Color accentColor3 = Color(0xFFADADAD);

TextStyle primaryFontBoldBlack = TextStyle(
  color: Colors.black,
  fontFamily: 'HelveticaNeueBold'
);

TextStyle primaryFontLightBlack = TextStyle(
  color: Colors.black,
  fontFamily: 'HelveticaNeueLight',
  letterSpacing: 1.25
);

TextStyle secondaryFontRegularBlack = TextStyle(
  color: Colors.black,
  fontFamily: 'OpenSans'
);