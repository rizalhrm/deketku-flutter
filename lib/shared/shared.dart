import 'dart:convert';
import 'dart:math';

import 'package:connectivity/connectivity.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:deketku/bloc/blocs.dart';
import 'package:location/location.dart';

part 'shared_value.dart';
part 'theme.dart';
part 'shared_method.dart';