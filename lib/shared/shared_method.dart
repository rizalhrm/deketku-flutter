part of 'shared.dart';

bool tokenIsExpired(int timestamp) {
  var now = DateTime.now();
  var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
  var diff = date.difference(now);
  var time = false;
  
  if (diff.inMinutes < 31) {
    time = true;
  }

  return time;
}

String encryptPassword(String password) {
  var bytes = utf8.encode(password);
  var digest = sha256.convert(bytes);  
  return digest.toString();
}

Future<bool> isDisconnected() async {
  var result = await Connectivity().checkConnectivity();
  if (result == ConnectivityResult.none) {
    return true;
  }
  return false;
}

const _chars = '1234567890AaBbCcDdEeFfGgHhIiJjKkL1234567890lMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

String replaceEmail(String params) {
  String email = params.toLowerCase();
  const start = "@";
  final startIndex = email.indexOf(start);
  final firstWord = email.substring(0, startIndex);
  final secondWord = email.substring(startIndex);
  final firstWordTruncated = firstWord.substring(2,firstWord.length);
  final twoInitialCharacters = firstWord.substring(0,2);
  final hiddenCharacters = firstWordTruncated.replaceAll(new RegExp(r'[a-z0-9._%+-]'), '*');
  return twoInitialCharacters+hiddenCharacters+secondWord;
}

int convertToEpochTime(String date) {
  return (DateTime.parse(date)).millisecondsSinceEpoch;
}

int getCurrentTimeStamp() {
  return new DateTime.now().millisecondsSinceEpoch;
}

// kesimpulan: saat memakai fitur buffer dll maka aplikasi memberitahu agar menyalakan permission location
Future<PermissionStatus> requestPermission() async {
  final Location location = Location();
  final PermissionStatus permissionGranted = await location.hasPermission();  
  if (permissionGranted == PermissionStatus.denied) {
    final PermissionStatus permissionRequestedResult = await location.requestPermission();
    if (permissionRequestedResult != PermissionStatus.granted) {
      // jika user menolak request permission
      return permissionRequestedResult;
    }
    return permissionGranted;
  }
  return permissionGranted;
}

Future<bool> requestService() async {
  final Location location = Location();
  final bool serviceEnabled = await location.serviceEnabled();
  if (serviceEnabled == null || !serviceEnabled) {
    final bool serviceRequestedResult = await location.requestService();
    if (!serviceRequestedResult) {
      return true;
    }
    return false;
  }
  return false;
}