part of 'pages.dart';

class CodeVerification extends StatefulWidget {
  final ForgotPasswordData forgotPasswordData;

  CodeVerification(this.forgotPasswordData);

  @override
  _CodeVerificationState createState() => _CodeVerificationState();
}

class _CodeVerificationState extends State<CodeVerification> {
  TextEditingController codeController = TextEditingController();
  bool isCheckingCode = false;
  bool isCodeValid = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.bloc<PageBloc>().add(GoToForgotPasswordPage(widget.forgotPasswordData));

        return;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () { context.bloc<PageBloc>().add(GoToForgotPasswordPage(widget.forgotPasswordData)); },
              );
            },
          ),
          title: Text(
            "Kode Verifikasi",
            style: primaryFontLightBlack.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              backgroundFrontPage,
              Container(
                height: double.infinity,
                width: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding:
                      EdgeInsets.fromLTRB(defaultMargin1, 120.0, defaultMargin1, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Masukkan kode verifikasi yang telah dikirim ke ${replaceEmail(widget.forgotPasswordData.email)}, cek spam atau email sampah jika tidak ada',
                        style: primaryFontBoldBlack.copyWith(fontSize: 18.0),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        alignment: Alignment.centerLeft,
                        decoration: textBox,
                        height: 60.0,
                        child: TextField(
                          onChanged: (text) {
                            setState(() {
                              isCodeValid = text.length >= 1;
                            });
                          },
                          controller: codeController,
                          keyboardType: TextInputType.text,
                          style: secondaryFontRegularBlack,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(top: 14.0),
                            prefixIcon: Icon(
                              Icons.vpn_key,
                              color: Colors.black45,
                            ),
                            hintText: 'Kode Verifikasi',
                            hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45)
                          ),
                        ),
                      ),
                      SizedBox(height: 20.0),
                      isCheckingCode 
                      ? SpinKitFadingCircle(
                          color: Color(0xFFFFFFFF),
                        )
                      : FloatingActionButton(
                          child: Icon(Icons.arrow_forward),
                          elevation: 2.0,
                          backgroundColor: isCodeValid ? accentColor2 : Color(0xFFBEBEBE),
                          onPressed: isCodeValid ? () {
                            setState(() {
                              isCheckingCode = true;
                            });

                            if (codeController.text == widget.forgotPasswordData.code) {
                              setState(() {
                                isCheckingCode = false;
                              });

                              context.bloc<PageBloc>().add(GoToResetPasswordPage(widget.forgotPasswordData));
                            } else {
                              setState(() {
                                isCheckingCode = false;
                              });

                              Flushbar(
                                duration: Duration(seconds: 2),
                                flushbarPosition: FlushbarPosition.TOP,
                                backgroundColor: Color(0xFFFF5C83),
                                message: "Kode verifikasi yang kamu masukkan salah",
                              )..show(context);
                            }
                          } : null,
                        ),
                    ]
                  )
                )
              )
            ],
          ),
        ),
      )
    );
  }
}