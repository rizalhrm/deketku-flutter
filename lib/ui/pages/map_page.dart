part of 'pages.dart';

class Place {
  Place(this.name, this.point);

  final String name;
  final LatLng point;
}

class MapPage extends StatefulWidget {
  final CoordinatesInitial coords;

  MapPage(this.coords);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  String rings;
  static final List<Place> _points = [];
  ProgressDialog pr;
  List<Marker> _markersOnMap=[];
  bool ready = false;
  final FeatureLayerController _featureLayerController = FeatureLayerController();
  final PopupController _popupLayerController = PopupController();
  bool loading=true;
  bool loadingAnalysis=false;

  @override
  void initState() {
    super.initState();
    _points.add(Place("Lokasi Kamu Saat Ini", LatLng(-6.367884,106.8263503)));
    _points.add(Place("Test", LatLng(-6.371008,106.8256633)));
    //widget.coords.latitude,widget.coords.longitude
    context.bloc<MapBloc>().add(MapWaiting());
  }

  void addMarker() {
    context.bloc<MapBloc>().add(AddMarker(
      _points[1].name,
      Marker(
        point: _points[1].point,
        builder: (BuildContext context) {
          return Image.asset("assets/images/logo/map-marker.png");
        }
      )
    ));
  }

  void testAnalysis() {
    setState(() {
      loadingAnalysis=true;
    });
    _featureLayerController.resetPolygonGraphics();
    context.bloc<MapBloc>().add(CreateAnalysisPolygon(
      "Buffer",
      "['200']",
      "Meters",
      "{'geometryType':'esriGeometryPoint','features':[{'geometry':{'x':106.8263503,'y':-6.367884,'spatialReference':{'wkid':4326}}}],'sr':{'wkid':4326}}"
    ));
  }

  void testAnalysis2() {
    setState(() {
      loadingAnalysis=true;
    });
    _featureLayerController.resetPolygonGraphics();
    context.bloc<MapBloc>().add(CreateAnalysisPolygon(
      "Driving",
      "20",
      "minutes",
      "{'geometryType':'esriGeometryPoint','features':[{'geometry':{'x':106.8263503,'y':-6.367884,'spatialReference':{'wkid':4326}}}],'sr':{'wkid':4326}}"
    ));
  }

  Widget _loading() {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.black26,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SpinKitFadingCircle(
              color: Colors.white,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                'Tunggu Sebentar...',
                style: primaryFontBoldBlack.copyWith(fontSize: 16.0, color: Colors.white),
              ),
            ),
          ],
        )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.bloc<PageBloc>().add(GoToMainPage(widget.coords));
        return;
      },
      child: Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.black12),
          child: BlocListener<MapBloc, MapState>(
            listener: (context, mapState) async {
              if (mapState is MapLoaded) {
                //check internet
                setState(() {
                  loading=true;
                });
                var statusDisconnected = await isDisconnected();
                if (statusDisconnected) {
                  final action =
                    await Dialogs.yesAbortDialog(
                      context,
                      titleAlertDisconnected,
                      bodyAlertDisconnected,
                      true
                    );
                  if (action == DialogAction.yes) {
                    AppSettings.openWIFISettings();
                  }
                } else {
                  context.bloc<MapBloc>().add(AddMarker(
                    _points[0].name,
                    Marker(
                      point: _points[0].point,
                      builder: (BuildContext context) {
                        return Image.asset("assets/images/logo/map-marker.png");
                      }
                    )
                  ));
                }
              }
              else if (mapState is MarkerLoaded) {
                setState(() {
                  loading=false;
                });
                for (var marker in mapState.poi) {
                  _markersOnMap.add(marker.marker);
                }
                setState(() {
                  loadingAnalysis=true;
                });
                context.bloc<MapBloc>().add(CreateAnalysisPolygon(
                  "Driving",
                  "10",
                  "minutes",
                  "{'geometryType':'esriGeometryPoint','features':[{'geometry':{'x':106.8263503,'y':-6.367884,'spatialReference':{'wkid':4326}}}],'sr':{'wkid':4326}}"
                ));
              }
              else if (mapState is PolygonState) {
                setState(() {
                  rings=mapState.rings;
                });
                _featureLayerController.renderPolygonGraphics(mapState.rings);
                setState(() {
                  loadingAnalysis=false;
                });
              }
              if(mapState is MapResponseError){
                // setState(() {
                //   loading=false;
                // });
                Flushbar(
                  duration: Duration(seconds: mapState.duration),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: mapState.message,
                )..show(context);
              }
            },
            child: loading ? _loading() : Stack(
              children: <Widget>[
                FlutterMap(
                  options: MapOptions(
                    center: _points.first.point,
                    zoom: 14.0,
                    interactive: true,
                    plugins: [FeatureLayerPlugin(), PopupMarkerPlugin()],
                    onTap: (_) => _popupLayerController.hidePopup(),
                  ),
                  layers: [
                    TileLayerOptions(
                      urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c'],
                      tileProvider: CachedNetworkTileProvider(),
                    ),
                    FeatureLayerOptions(
                      featureLayerController: _featureLayerController,
                      rings: rings,
                      render: (){
                        return PolygonOptions(
                          color: Colors.black12,
                          borderStrokeWidth: 2,
                          borderColor: Colors.blueAccent
                        );
                      },
                    ),
                    PopupMarkerLayerOptions(
                      markers: _markersOnMap,
                      popupSnap: PopupSnap.top,
                      popupController: _popupLayerController,
                      popupBuilder: (BuildContext _, Marker marker) {
                        final title = _points.firstWhere((a) => a.point == marker.point, orElse: () => null);
                        return MapPopup(title.name);
                      },
                    ),
                  ]
                ),
                loadingAnalysis ? _loading() : SizedBox(),
              ],
            ),
          ),
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FloatingActionButton(
              child: Text('Buffer'),
              elevation: 2.0,
              backgroundColor: accentColor1,
              onPressed: () {
                testAnalysis();
              },
            ),
            FloatingActionButton(
              child: Text('Marker'),
              elevation: 2.0,
              backgroundColor: accentColor2,
              onPressed: () {
                addMarker();
              },
            ),
            FloatingActionButton(
              child: Text('Driving'),
              elevation: 2.0,
              backgroundColor: primaryColor,
              onPressed: () {
                testAnalysis2();
              },
            ),
            FloatingActionButton(
              child: Text('Poi'),
              elevation: 2.0,
              backgroundColor: accentColor1,
              onPressed: () {
                
              },
            ),
          ],
        ) 
      ),
    );
  }
}