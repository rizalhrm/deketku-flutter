part of 'pages.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  @override
  void initState() {
    super.initState();
    requestPermission();
    requestService();
  }
  
  bool passwordVisible = true;
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isUsernameValid = false;
  bool isPasswordValid = false;
  bool isSigningIn = false;
  String _token;

  Widget _buildUsernameTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Username',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: textBox,
          height: 60.0,
          child: TextField(
            onChanged: (text) {
              setState(() {
                isUsernameValid = text.length >= 1;
              });
            },
            controller: usernameController,
            keyboardType: TextInputType.text,
            style: secondaryFontRegularBlack,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person,
                color: Colors.black45,
              ),
              hintText: 'Ketik Username kamu',
              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45)
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: textBox,
          height: 60.0,
          child: TextField(
            onChanged: (text) {
              setState(() {
                isPasswordValid = text.length >= 1;
              });
            },
            controller: passwordController,
            obscureText: passwordVisible,
            style: secondaryFontRegularBlack,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.black45,
              ),
              hintText: 'Ketik Password kamu',
              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45),
              suffixIcon: IconButton(
                icon: Icon(
                  passwordVisible
                  ? Icons.visibility_off
                  : Icons.visibility,
                  color: Colors.black45,
                ),
                onPressed: () {
                  setState(() {
                    passwordVisible = !passwordVisible;
                  });
                }
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: !isSigningIn ? () {
          context.bloc<PageBloc>().add(GoToForgotPasswordPage(ForgotPasswordData()));
        } : () async {
          await AlertWarning.okayDialog(context, "Silahkan tunggu sebentar...", "Saat ini masih proses login", false);
        },
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          'Lupa Password?',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: isSigningIn 
        ? SpinKitFadingCircle(
            color: Color(0xFFFFFFFF),
          )
        : RaisedButton(
            elevation: 5.0,
            onPressed: isUsernameValid && isPasswordValid ? () async {
              setState(() {
                isSigningIn = true;
              });

              var statusDisconnected = await isDisconnected();
              if (statusDisconnected) {
                final action =
                  await Dialogs.yesAbortDialog(
                    context,
                    titleAlertDisconnected,
                    bodyAlertDisconnected,
                    true
                  );
                if (action == DialogAction.yes) {
                  AppSettings.openWIFISettings();
                }
                setState(() {
                  isSigningIn = false;
                });
              } else {
                context.bloc<UserBloc>().add(RefreshTokenAdmin(
                  "rizal", "rizal1224", "admin", "user"
                ));
              }
            } : null, 
            padding: EdgeInsets.all(15.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            color: isUsernameValid && isPasswordValid
                ? Color(0xFFFAFAFA)
                : Color(0xFFBEBEBE),
            child: Text(
              'LOGIN',
              style: primaryFontBoldBlack.copyWith(
                color: isUsernameValid && isPasswordValid
                    ? Colors.black
                    : Color(0xFFBEBEBE),
                letterSpacing: 1.5,
                fontSize: 18.0,
              )
            ),
          ),
    );
  }

  Widget _buildSignupBtn() {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: !isSigningIn ? () {
          context.bloc<PageBloc>().add(GoToRegistrationPage());
        } : () async {
          await AlertWarning.okayDialog(context, "Silahkan tunggu sebentar...", "Saat ini masih proses login", false);
        }, 
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        color: Color(0xFFFAFAFA),
        child: Text(
          'Buat Akun Baru',
          style: primaryFontBoldBlack.copyWith(
            color: Colors.black,
            letterSpacing: 1,
            fontSize: 16.0,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: isSigningIn ? () async {
        await AlertWarning.okayDialog(context, "Silahkan tunggu sebentar...", "Saat ini masih proses login", false);
        return;
      } : () {
        return;
      },
      child: Scaffold(
        body: BlocListener<UserBloc, UserState>(
          listener: (_, userState) async {
            if(userState is TokenLoadedAdmin) {
              if (userState.tokenAdmin.code == 200) {
                setState(() {
                  _token = userState.tokenAdmin.token;
                });
                context.bloc<UserBloc>().add(GetUserCount(
                  usernameController.text, userState.tokenAdmin.token
                ));
              }
              else {
                setState(() {
                  isSigningIn = false;
                });
                Flushbar(
                  duration: Duration(seconds: 2),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Error. Silahkan coba lagi.",
                )..show(context);
              }
            }
            else if(userState is CountLoaded) {
              var count = userState.count.count;
              if (count == null) {
                if (userState.count.error.message == "Invalid Token") {
                  context.bloc<UserBloc>().add(ReloadUserCount(
                    usernameController.text, _token
                  ));
                } else {
                  setState(() {
                    isSigningIn = false;
                  });
                  Flushbar(
                    duration: Duration(seconds: 3),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Error. Silahkan coba lagi.",
                  )..show(context);
                }
              }
              else {
                if (count > 0) {
                  context.bloc<UserBloc>().add(LoginUser(
                    usernameController.text, passwordController.text, "user", "user"
                  ));
                }
                else {
                  setState(() {
                    isSigningIn = false;
                  });
                  Flushbar(
                    duration: Duration(seconds: 4),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Username atau Password salah.\nSilahkan periksa kembali username atau password yang kamu masukkan.",
                  )..show(context);
                }
              }
            }
            else if(userState is CountReloaded) {
              var count = userState.recount.count;
              if (count == null) {
                setState(() {
                  isSigningIn = false;
                });
                Flushbar(
                  duration: Duration(seconds: 3),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Error. Silahkan coba lagi.",
                )..show(context);
              }
              else {
                if (count > 0) {
                  context.bloc<UserBloc>().add(LoginUser(
                    usernameController.text, passwordController.text, "user", "user"
                  ));
                }
                else {
                  setState(() {
                    isSigningIn = false;
                  });
                  Flushbar(
                    duration: Duration(seconds: 4),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Username atau Password salah.\nSilahkan periksa kembali username atau password yang kamu masukkan.",
                  )..show(context);
                }
              }
            }
            else if(userState is LoginSuccess) {
              if (userState.login.code == 200) {
                SharedPreferences prefsToken = await SharedPreferences.getInstance();
                prefsToken.setString('token', userState.login.token);

                SharedPreferences prefsExpires = await SharedPreferences.getInstance();
                prefsExpires.setInt('expires', userState.login.expires);

                SharedPreferences prefsUsername = await SharedPreferences.getInstance();
                prefsUsername.setString('username', usernameController.text);

                String password = encryptPassword(passwordController.text);
                SharedPreferences prefsPassword = await SharedPreferences.getInstance();
                prefsPassword.setString('password', password);

                setState(() {
                  isSigningIn = false;
                });
                
                context.bloc<TokenBloc>().add(GetToken());
                context.bloc<PageBloc>().add(GoToMainPage(CoordinatesInitial()));
              }
              else if (userState.login.code == 401 || userState.login.details == "Invalid credentials") {
                setState(() {
                  isSigningIn = false;
                });
                Flushbar(
                  duration: Duration(seconds: 4),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Username atau Password salah.\nSilahkan periksa kembali username atau password yang kamu masukkan.",
                )..show(context);
              }
              else {
                setState(() {
                  isSigningIn = false;
                });
                Flushbar(
                  duration: Duration(seconds: 4),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Username atau Password salah.\nSilahkan periksa kembali username atau password yang kamu masukkan.",
                )..show(context);
              }
            }
            if(userState is ResponseError){
              setState(() {
                isSigningIn = false;
              });
              Flushbar(
                duration: Duration(seconds: userState.duration),
                flushbarPosition: FlushbarPosition.TOP,
                backgroundColor: Color(0xFFFF5C83),
                message: userState.message,
              )..show(context);
            }
          },
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              children: <Widget>[
                backgroundFrontPage,
                Container(
                  height: double.infinity,
                  child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    padding:
                        EdgeInsets.fromLTRB(defaultMargin1, 100.0, defaultMargin1, 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Sign In',
                          style: primaryFontBoldBlack.copyWith(fontSize: 32.0),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        _buildUsernameTF(),
                        SizedBox(
                          height: 30.0,
                        ),
                        _buildPasswordTF(),
                        _buildForgotPasswordBtn(),
                        _buildLoginBtn(),
                        _buildSignupBtn(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}