part of 'pages.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    context.bloc<TokenBloc>().add(GetToken());
  }

  Widget _deketkuSplashScreen() {
    return Scaffold(
      body: Container(
        color: primaryColor,
        // decoration: BoxDecoration(
        //   gradient: LinearGradient(
        //     begin: Alignment.topRight,
        //     end: Alignment.bottomCenter,
        //     colors: [ 
        //       primaryColor
        //     ],
        //   ),
        // ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height / 3.5,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                "assets/images/logo/logo_splashscreen.png",
                alignment: Alignment.center,
              ),
              // decoration: BoxDecoration(
              //   image: DecorationImage(
              //     image: AssetImage(
              //       'assets/images/logo/logo_splashscreen.png'
              //     ),
              //     fit: BoxFit.contain
              //   ),
              // ),
            ),
          ],
        ),
      ), 
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TokenBloc, TokenState>(
      builder: (_, tokenState) {
        if (tokenState is TokenLoadedUser) {
          var _duration = Duration(seconds: 5);
          String username = tokenState.tokenUser.username;
          String password = tokenState.tokenUser.password;

          if (username == null && password == null) {
            if (!(prevPageEvent is GoToLoginPage)) {
              prevPageEvent = GoToLoginPage();
              Timer(_duration, () => context.bloc<PageBloc>().add(prevPageEvent));
            }
          } 
          else {
            if (!(prevPageEvent is GoToMainPage)) {
              prevPageEvent = GoToMainPage(CoordinatesInitial());
              Timer(_duration, () => context.bloc<PageBloc>().add(prevPageEvent));
            }
          }

          return BlocBuilder<PageBloc, PageState>(
            builder: (_, pageState) => 
              (pageState is OnLoginPage) ? SignIn()
              : (pageState is OnMainPage) ? MainPage(pageState.coords)
              : (pageState is OnForgotPasswordPage) ? ForgotPassword(pageState.forgotPasswordData)
              : (pageState is OnCodeVerificationPage) ? CodeVerification(pageState.forgotPasswordData)
              : (pageState is OnResetPasswordPage) ? ResetPassword(pageState.forgotPasswordData)
              : (pageState is OnRegistrationPage) ? Registration()
              : (pageState is OnMapPage) ? MapPage(pageState.coords)
              : _deketkuSplashScreen()
            );
        }
        else {
          return _deketkuSplashScreen();
        }
      }
    );
  }
}