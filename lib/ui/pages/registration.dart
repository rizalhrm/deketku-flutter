part of 'pages.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  final Location location = Location();
  LocationData _location;
  String _errorLocation;
  ProgressDialog pr;
  List<Country> _dataCountry = List<Country>.from(json.decode(countryJsonString).map((x) => Country.fromJson(x)));
  List<Gender> _dataGender = Gender.getGenders();
  List<DropdownMenuItem<Country>> _dropdownMenuItems;
  List<DropdownMenuItem<Gender>> _dropdownGenderItems;
  Country _selectedCountry;
  Gender _selectedGender;
  DateTime _dateOfBirth;
  int _dateOfBirthEpochTime;
  String _dateOfBirthString;
  DateTimePickerLocale _locale = DateTimePickerLocale.en_us;
  File _image;
  final picker = ImagePicker();
  TextEditingController usernameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController fullnameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  bool loading = false;
  bool passwordVisible = true;
  bool confirmPasswordVisible = true;
  RegExp validateUsername = new RegExp(r'^[a-z0-9_]+$');
  RegExp validateFullname = new RegExp(r'^[a-zA-Z ]*$');
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  String _deviceModel;
  String _advertisingId;
  String _token;
  String _userId;

  @override
  void initState() {
    super.initState();
    _dateOfBirth = DateTime.parse(INIT_DATE);
    _dropdownMenuItems = buildDropdownMenuItems(_dataCountry);
    _dropdownGenderItems = buildDropdownGenderItems(_dataGender);
    _selectedCountry = _dropdownMenuItems[0].value;
    _selectedGender = _dropdownGenderItems[0].value;
    _getLocation();
    _initDeviceModelState();
  }

  Future<void> _getLocation() async {
    setState(() {
      _errorLocation = null;
    });
    try {
      LocationData locationResult = await location.getLocation();
      setState(() {
        _location = locationResult;
      });
    } on PlatformException catch (err) {
      setState(() {
        _errorLocation = err.code;
      });
    }
  }

  Future<void> _initDeviceModelState() async {
    String advertisingId;

    try {
      if (Platform.isAndroid) {
        _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
      advertisingId = await AdvertisingId.id;
    } on PlatformException {
      print('Error: Failed to get platform version.');
      print('Failed to get platform version.');
    }
    
    if (!mounted) return;

    setState(() {
      _advertisingId = advertisingId;
    });
  }

  _readAndroidBuildData(AndroidDeviceInfo build) {
    setState(() {
      _deviceModel = build.model;
    });
  }

  _readIosDeviceInfo(IosDeviceInfo data) {
    setState(() {
      _deviceModel = data.model;
    });
  }

  Future getCameraImage() async {
    final image = await picker.getImage(source: ImageSource.camera, maxWidth: 400, maxHeight: 600);

    setState(() {
      _image = File(image.path);
    });
    Navigator.pop(context);
  }

  Future getGalleryImage() async {
    final image = await picker.getImage(source: ImageSource.gallery, maxWidth: 400, maxHeight: 600);

    setState(() {
      _image = File(image.path);
    });
    Navigator.pop(context);
  }

  void _showDateTimePicker() {
    DatePicker.showDatePicker(
      context,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text('Selesai'),
        cancel: Text('Batal'),
      ),
      minDateTime: DateTime.parse(MIN_DATE),
      maxDateTime: DateTime.parse(maxDate),
      initialDateTime: _dateOfBirth,
      dateFormat: formatDate,
      locale: _locale,
      onClose: () => debugPrint("----- onClose -----"),
      onCancel: () => debugPrint('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateOfBirth = dateTime;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _dateOfBirth = dateTime;
          _dateOfBirthString = "${_dateOfBirth.year}-${_dateOfBirth.month.toString().padLeft(2, '0')}-${_dateOfBirth.day.toString().padLeft(2, '0')}";
          _dateOfBirthEpochTime = convertToEpochTime(_dateOfBirthString);
        });
      },
    );
  }

  List<DropdownMenuItem<Country>> buildDropdownMenuItems(List countries) {
    List<DropdownMenuItem<Country>> items = List();
    for (Country country in countries) {
      items.add(
        DropdownMenuItem(
          value: country,
          child: Container(
            width: 250,
            child: Text(
              country.name,
              overflow: TextOverflow.ellipsis,
              style: secondaryFontRegularBlack.copyWith(color: Colors.black),
            )
          ),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Country selectedCountry) {
    setState(() {
      _selectedCountry = selectedCountry;
    });
  }

  List<DropdownMenuItem<Gender>> buildDropdownGenderItems(List genders) {
    List<DropdownMenuItem<Gender>> items = List();
    for (Gender gender in genders) {
      items.add(
        DropdownMenuItem(
          value: gender,
          child: Container(
            width: 250,
            child: Text(
              gender.jenis,
              overflow: TextOverflow.ellipsis,
              style: secondaryFontRegularBlack.copyWith(color: Colors.black),
            )
          ),
        ),
      );
    }
    return items;
  }

  onChangeDropdownGenderItem(Gender selectedGender) {
    print(selectedGender.id);
    setState(() {
      _selectedGender = selectedGender;
    });
  }

  _removeUser() {
    RegistrationServices.removeUserFromArcGISServer(_token, usernameController.text).then((_){
      setState(() {
        loading = false;
      });
      pr.hide();
      Flushbar(
        duration: Duration(seconds: 4),
        flushbarPosition: FlushbarPosition.TOP,
        backgroundColor: Color(0xFFFF5C83),
        message: "Error!. Gagal Registrasi Akun.\nSilahkan coba lagi.",
      )..show(context);
    });
  }

  Widget _buildPhotoProfileCircle() {
    return Container(
      width: 90,
      height: 105,
      child: Stack(
        children: <Widget>[
          Container(
            height: 90,
            width: 90,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: (_image == null)
                  ? AssetImage("assets/images/logo/user_pic.png")
                  : FileImage(_image),
                fit: BoxFit.cover
              )
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: () async {
                  if (_image == null) {
                    ActionSheet.cupertino(
                      context,
                      "Upload Foto Profil",
                      getCameraImage,
                      getGalleryImage,
                      "Kamera",
                      "Galeri"
                    );
                  } else {
                    _image = null;
                  }

                  setState(() {});
                },
                child: Container(
                  height: 28,
                  width: 28,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage((_image == null)
                        ? "assets/images/logo/btn_add_photo.png"
                        : "assets/images/logo/btn_del_photo.png"
                      )
                    )
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildUsernameTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Username',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: textBox,
          height: 60.0,
          child: TextField(
            controller: usernameController,
            keyboardType: TextInputType.text,
            style: secondaryFontRegularBlack,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person,
                color: Colors.black45,
              ),
              hintText: 'Ketik Username kamu',
              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45)
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: textBox,
          height: 60.0,
          child: TextField(
            controller: emailController,
            keyboardType: TextInputType.emailAddress,
            style: secondaryFontRegularBlack,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.black45,
              ),
              hintText: 'Ketik Email kamu',
              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45)
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildFullnameTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nama Lengkap',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: textBox,
          height: 60.0,
          child: TextField(
            controller: fullnameController,
            keyboardType: TextInputType.text,
            style: secondaryFontRegularBlack,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person,
                color: Colors.black45,
              ),
              hintText: 'Ketik Nama Lengkap kamu',
              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45)
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDatePicker() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Tanggal Lahir',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              _dateOfBirthEpochTime != null || _dateOfBirthString != null ? _dateOfBirthString : '0000-00-00',
              style: primaryFontBoldBlack.copyWith(fontSize: 15.0),
              textAlign: TextAlign.center,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: FloatingActionButton.extended(
                icon: Icon(Icons.date_range),
                elevation: 2.0,
                backgroundColor: accentColor2,
                onPressed: () => _showDateTimePicker(),
                label: Text('Ubah')
              ),
            ),
          ]
        ),
      ],
    );
  }

  Widget _buildCountryComboBox() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Kebangsaan',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Container(
          alignment: Alignment.center,
          decoration: textBox,
          height: 55.0,
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                value: _selectedCountry,
                items: _dropdownMenuItems,
                onChanged: onChangeDropdownItem,
              )
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildGenderComboBox() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Jenis Kelamin',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Container(
          alignment: Alignment.center,
          decoration: textBox,
          height: 55.0,
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                value: _selectedGender,
                items: _dropdownGenderItems,
                onChanged: onChangeDropdownGenderItem,
              )
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPhoneNumber() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nomor Handphone',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: textBox,
          height: 60.0,
          child: TextField(
            controller: phoneController,
            keyboardType: TextInputType.phone,
            style: secondaryFontRegularBlack,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.smartphone,
                color: Colors.black45,
              ),
              hintText: 'Ketik No. HP kamu',
              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45)
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: textBox,
          height: 60.0,
          child: TextField(
            controller: passwordController,
            obscureText: passwordVisible,
            style: secondaryFontRegularBlack,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.black45,
              ),
              hintText: 'Buat Password',
              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45),
              suffixIcon: IconButton(
                icon: Icon(
                  passwordVisible
                  ? Icons.visibility_off
                  : Icons.visibility,
                  color: Colors.black45,
                ),
                onPressed: () {
                  setState(() {
                    passwordVisible = !passwordVisible;
                  });
                }
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildConfirmPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Konfirmasi Password',
          style: primaryFontLightBlack.copyWith(fontSize: 15.0),
        ),
        SizedBox(height: 6.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: textBox,
          height: 60.0,
          child: TextField(
            controller: confirmPasswordController,
            obscureText: confirmPasswordVisible,
            style: secondaryFontRegularBlack,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.black45,
              ),
              hintText: 'Konfirmasi Password',
              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45),
              suffixIcon: IconButton(
                icon: Icon(
                  confirmPasswordVisible
                  ? Icons.visibility_off
                  : Icons.visibility,
                  color: Colors.black45,
                ),
                onPressed: () {
                  setState(() {
                    confirmPasswordVisible = !confirmPasswordVisible;
                  });
                }
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSubmitBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () async {
          if (!(usernameController.text.trim() != "" &&
              emailController.text.trim() != "" &&
              fullnameController.text.trim() != "" &&
              _dateOfBirthString != "0000-00-00" &&
              phoneController.text.trim() != "" &&
              passwordController.text.trim() != "" &&
              confirmPasswordController.text.trim() != "" &&
              _image != null)) {
            Flushbar(
              duration: Duration(milliseconds: 1500),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Diharuskan mengisi semua data !",
            )..show(context);
          }
          else if (usernameController.text.length < 4) {
            Flushbar(
              duration: Duration(milliseconds: 1500),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Username minimal 4 karakter",
            )..show(context);
          }
          else if (!validateUsername.hasMatch(usernameController.text)) {
            Flushbar(
              duration: Duration(milliseconds: 3000),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Username tidak boleh ada huruf Kapital, spasi dan simbol (kecuali tanda '_')",
            )..show(context);
          }
          else if (!EmailValidator.validate(
              emailController.text)) {
            Flushbar(
              duration: Duration(milliseconds: 1500),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Email tidak valid",
            )..show(context);
          }
          else if (!validateFullname.hasMatch(fullnameController.text)) {
            Flushbar(
              duration: Duration(milliseconds: 3000),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Nama lengkap tidak boleh ada angka dan simbol",
            )..show(context);
          }
          else if (phoneController.text.length < 10 || phoneController.text.length > 13) {
            Flushbar(
              duration: Duration(milliseconds: 3000),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Nomor Handphone harus memiliki antara 10 sampai 13 digit angka",
            )..show(context);
          }
          else if (passwordController.text != confirmPasswordController.text) {
            Flushbar(
              duration: Duration(milliseconds: 3000),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Password dan konfirmasi password tidak sama.",
            )..show(context);
          }
          else if (passwordController.text.length < 5) {
            Flushbar(
              duration: Duration(milliseconds: 1500),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Password minimal 5 karakter",
            )..show(context);
          }
          else {
            setState(() {
              loading = true;
              pr.show();
            });

            var statusDisconnected = await isDisconnected();
            if (statusDisconnected) {
              final action =
                await Dialogs.yesAbortDialog(
                  context,
                  titleAlertDisconnected,
                  bodyAlertDisconnected,
                  true
                );
              if (action == DialogAction.yes) {
                AppSettings.openWIFISettings();
              }
              setState(() {
                loading = false;
              });
              pr.hide();
            } else {
              context.bloc<UserBloc>().add(RefreshTokenAdmin(
                "rizal", "rizal1224", "admin", "user"
              ));
            }
          }
        }, 
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        color: Color(0xFFFAFAFA),
        child: Text(
          'Buat Akun Deketku',
          style: primaryFontBoldBlack.copyWith(
            color: Colors.black,
            letterSpacing: 1.5,
            fontSize: 18.0,
          )
        ),
      ), 
    );
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: false,
    );

    pr.style(
      message: 'Tunggu sebentar...',
      borderRadius: 10.0,
      backgroundColor: Colors.white,
      progressWidget: SpinKitFadingCircle(color: primaryColor),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
      messageTextStyle: primaryFontLightBlack.copyWith(color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600),
    );

    return WillPopScope(
      onWillPop: () async {
        context.bloc<PageBloc>().add(GoToLoginPage());
        return;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: !loading ? () { context.bloc<PageBloc>().add(GoToLoginPage()); } : () async {
                  await AlertWarning.okayDialog(context, "Silahkan tunggu sebentar...", "Saat ini masih proses pendaftaran akun baru", false);
                },
              );
            },
          ),
          title: Text(
            "Registrasi Akun",
            style: primaryFontLightBlack.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        body: BlocListener<UserBloc, UserState>(
          listener: (_, userState) async {
            if(userState is TokenLoadedAdmin) {
              if (userState.tokenAdmin.code == 200) {
                setState(() {
                  _token = userState.tokenAdmin.token;
                });
                context.bloc<UserBloc>().add(GetUserCount(
                  usernameController.text, userState.tokenAdmin.token
                ));
              }
              else {
                setState(() {
                  loading = false;
                });
                pr.hide();
                Flushbar(
                  duration: Duration(seconds: 3),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Error.\nSilahkan coba lagi.",
                )..show(context);
              }
            }
            else if(userState is CountLoaded) {
              var count = userState.count.count;
              if (count == null) {
                if (userState.count.error.message == "Invalid Token") {
                  context.bloc<UserBloc>().add(ReloadUserCount(
                    usernameController.text, _token
                  ));
                } else {
                  setState(() {
                    loading = false;
                  });
                  pr.hide();
                  Flushbar(
                    duration: Duration(seconds: 3),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Error. Silahkan coba lagi.",
                  )..show(context);
                }
              }
              else {
                if (count > 0) {
                  setState(() {
                    loading = false;
                  });
                  pr.hide();
                  Flushbar(
                    duration: Duration(seconds: 4),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Username ${usernameController.text} sudah ada.\nSilahkan gunakan username berbeda.",
                  )..show(context);
                }
                else {
                  context.bloc<UserBloc>().add(AddUserToArcGISServer(
                    _token,
                    passwordController.text,
                    usernameController.text,
                    fullnameController.text,
                    emailController.text
                  ));
                }
              }
            }
            else if(userState is CountReloaded) {
              var count = userState.recount.count;
              if (count == null) {
                setState(() {
                  loading = false;
                });
                pr.hide();
                Flushbar(
                  duration: Duration(seconds: 3),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Error. Silahkan coba lagi.",
                )..show(context);
              }
              else {
                if (count > 0) {
                  setState(() {
                    loading = false;
                  });
                  pr.hide();
                  Flushbar(
                    duration: Duration(seconds: 4),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Username ${usernameController.text} sudah ada.\nSilahkan gunakan username berbeda.",
                  )..show(context);
                }
                else {
                  context.bloc<UserBloc>().add(AddUserToArcGISServer(
                    _token,
                    passwordController.text,
                    usernameController.text,
                    fullnameController.text,
                    emailController.text
                  ));
                }
              }
            }
            else if (userState is ResultAddUserToArcGISServer) {
              context.bloc<UserBloc>().add(AddUsersToRole(
                _token,
                usernameController.text
              ));
            }
            else if (userState is ResultAddUsersToRole) {
              if (userState.resultAddRole.contains('success')) {
                String userId = "user${getCurrentTimeStamp().toString()}";
                setState(() {
                  _userId  = userId;
                });
                String featureForAddUser;
                String favoritetag = '[{"value":"9|atm"}]';
                if (Platform.isAndroid) {
                  featureForAddUser = "[{'attributes':{'userid':'$userId','username':'${usernameController.text}','email':'${emailController.text}','dob':$_dateOfBirthEpochTime,'alias':'${fullnameController.text}','phonenumber':'${phoneController.text}','ismerchant':0,'emailisverified':0,'phoneisverified':0,'usertier':0,'userpoint':0,'nationality':${_selectedCountry.id},'gender':${_selectedGender.id},'u_date':${getCurrentTimeStamp()},'favoritetag': '$favoritetag','defaultsearchby':1,'defaultradiusvalue':500,'defaultdrivingtimevalue':5,'defaultdrivingdistance':400,'aaid':'$_advertisingId','status':1}}]";
                } else if (Platform.isIOS) {
                  featureForAddUser = "[{'attributes':{'userid':'$userId','username':'${usernameController.text}','email':'${emailController.text}','dob':$_dateOfBirthEpochTime,'alias':'${fullnameController.text}','phonenumber':'${phoneController.text}','ismerchant':0,'emailisverified':0,'phoneisverified':0,'usertier':0,'userpoint':0,'nationality':${_selectedCountry.id},'gender':${_selectedGender.id},'u_date':${getCurrentTimeStamp()},'favoritetag': '$favoritetag','defaultsearchby':1,'defaultradiusvalue':500,'defaultdrivingtimevalue':5,'defaultdrivingdistance':400,'idfa':'$_advertisingId','status':1}}]";
                }
                
                context.bloc<UserBloc>().add(AddUserToFeaturesServices(
                  _token, featureForAddUser, 16
                ));
              }
              else {
                _removeUser();
              }
            }
            else if (userState is ResultsAddUserToFeaturesServices) {
              if (userState.addUserModel.status == "success") {
                int objectId = userState.addUserModel.addResults[0].objectId;
                String pathPhoto = _image.path.toString();
                var arr = pathPhoto.split('/');
                int i = arr.length-1;
                var ext = arr[i].split('.');
                int j = ext.length-1;
                String dir = (await getApplicationDocumentsDirectory()).path;

                String newPath = path.join(dir, '${_userId}_deketku_${getRandomString(5)}.${ext[j]}');
                File _photo = await File(_image.path).copy(newPath);
                
                if (_location != null && _errorLocation == null) {
                  String feature;
                  if (Platform.isAndroid) {
                    feature = "[{'geometry':{'x':${_location.longitude},'y':${_location.latitude},'z':${_location.altitude}},'attributes':{'userid':'$_userId','longitude':${_location.longitude},'latitude':${_location.latitude},'altitude':${_location.altitude},'u_date':${getCurrentTimeStamp()},'aaid':'$_advertisingId','devicemodel':'$_deviceModel'}}]";
                  } else if (Platform.isIOS) {
                    feature = "[{'geometry':{'x':${_location.longitude},'y':${_location.latitude},'z':${_location.altitude}},'attributes':{'userid':'$_userId','longitude':${_location.longitude},'latitude':${_location.latitude},'altitude':${_location.altitude},'u_date':${getCurrentTimeStamp()},'idfa':'$_advertisingId','devicemodel':'$_deviceModel'}}]";
                  }
                  RegistrationServices.addFeatures(_token, feature, 0).then((_){});
                }

                context.bloc<UserBloc>().add(UploadPhotoProfile(
                  _token, _photo, objectId
                ));
              } else {
                _removeUser();
              }
            }
            else if (userState is ResultUploadPhotoProfile) {
              if (userState.resultUpload.contains('success')) {
                context.bloc<UserBloc>().add(LoginUser(
                  usernameController.text, passwordController.text, "user", "user"
                ));
              }
              else {
                setState(() {
                  _selectedCountry = _dropdownMenuItems[0].value;
                  _selectedGender = _dropdownGenderItems[0].value;
                  _dateOfBirthEpochTime = null;
                  _dateOfBirthString = null;
                  loading = false;
                  _image = null;
                });
                pr.hide();
                Flushbar(
                  duration: Duration(seconds: 4),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Colors.green[200],
                  message: "Gagal Upload Foto Profile.\nSilahkan login dengan akun baru kamu.",
                )..show(context);
                context.bloc<PageBloc>().add(GoToLoginPage());
              }
            }
            else if(userState is LoginSuccess) {
              if (userState.login.code == 200) {
                SharedPreferences prefsToken = await SharedPreferences.getInstance();
                prefsToken.setString('token', userState.login.token);

                SharedPreferences prefsExpires = await SharedPreferences.getInstance();
                prefsExpires.setInt('expires', userState.login.expires);

                SharedPreferences prefsUsername = await SharedPreferences.getInstance();
                prefsUsername.setString('username', usernameController.text);

                String password = encryptPassword(passwordController.text);
                SharedPreferences prefsPassword = await SharedPreferences.getInstance();
                prefsPassword.setString('password', password);

                setState(() {
                  _selectedCountry = _dropdownMenuItems[0].value;
                  _selectedGender = _dropdownGenderItems[0].value;
                  _dateOfBirthEpochTime = null;
                  _dateOfBirthString = null;
                  loading = false;
                  _image = null;
                });
                pr.hide();
                context.bloc<TokenBloc>().add(GetToken());
                context.bloc<PageBloc>().add(GoToMainPage(CoordinatesInitial()));
              }
              else {
                setState(() {
                  _selectedCountry = _dropdownMenuItems[0].value;
                  _selectedGender = _dropdownGenderItems[0].value;
                  _dateOfBirthEpochTime = null;
                  _dateOfBirthString = null;
                  loading = false;
                  _image = null;
                });
                pr.hide();
                Flushbar(
                  duration: Duration(seconds: 4),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Colors.green[200],
                  message: "Silahkan login dengan akun baru kamu.",
                )..show(context);
                context.bloc<PageBloc>().add(GoToLoginPage());
              }
            }
            if(userState is ResponseError){
              setState(() {
                loading = false;
              });
              pr.hide();
              Flushbar(
                duration: Duration(seconds: userState.duration),
                flushbarPosition: FlushbarPosition.TOP,
                backgroundColor: Color(0xFFFF5C83),
                message: userState.message,
              )..show(context);
            }
          },
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              children: <Widget>[
                backgroundFrontPage,
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    padding:
                        EdgeInsets.fromLTRB(defaultMargin1, 20.0, defaultMargin1, 12),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        _buildPhotoProfileCircle(),
                        SizedBox(height: 10.0),
                        _buildUsernameTF(),
                        SizedBox(height: 12.0),
                        _buildEmailTF(),
                        SizedBox(height: 12.0),
                        _buildFullnameTF(),
                        SizedBox(height: 12.0),
                        _buildDatePicker(),
                        SizedBox(height: 12.0),
                        _buildCountryComboBox(),
                        SizedBox(height: 12.0),
                        _buildGenderComboBox(),
                        SizedBox(height: 12.0),
                        _buildPhoneNumber(),
                        SizedBox(height: 12.0),
                        _buildPasswordTF(),
                        SizedBox(height: 12.0),
                        _buildConfirmPasswordTF(),
                        SizedBox(height: 10.0),
                        _buildSubmitBtn()
                      ],
                    ),
                  ),
                )
              ]
            ),
          )
        )
      )
    );
  }
}