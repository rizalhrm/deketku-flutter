import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:device_info/device_info.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:advertising_id/advertising_id.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';

import 'package:deketku/plugin/plugin.dart';
import 'package:deketku/models/models.dart';
import 'package:deketku/bloc/blocs.dart';
import 'package:deketku/services/services.dart';
import 'package:deketku/shared/shared.dart';
import 'package:deketku/ui/widgets/widgets.dart';

part 'splash_screen.dart';
part 'sign_in.dart';
part 'forgot_password.dart';
part 'main_page.dart';
part 'code_verification.dart';
part 'reset_password.dart';
part 'registration.dart';
part 'map_page.dart';