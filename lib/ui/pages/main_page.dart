part of 'pages.dart';

class MainPage extends StatefulWidget {
  final CoordinatesInitial coords;

  MainPage(this.coords);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final Location location = Location();
  LocationData _location;
  String _errorLocation;

  @override
  void initState() {
    super.initState();
    requestPermission();
    requestService();
    _getLocation();
  }

  Future<void> _getLocation() async {
    setState(() {
      _errorLocation = null;
    });
    try {
      LocationData locationResult = await location.getLocation();
      setState(() {
        _location = locationResult;
      });
    } on PlatformException catch (err) {
      setState(() {
        _errorLocation = err.code;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Main Page"),
      ),
      body: BlocListener<UserBloc, UserState>(
        listener: (_, userState) async {
          if(userState is LoginSuccess) {
            print(userState.login.token);
          }
        },
        child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark.copyWith(statusBarColor: primaryColor),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  elevation: 5.0,
                  onPressed: () async {
                    if (_location != null && _errorLocation == null) {
                      widget.coords.latitude = _location.latitude;
                      widget.coords.longitude = _location.longitude;
                    }

                    context.bloc<PageBloc>().add(GoToMapPage(widget.coords));
                  }, 
                  padding: EdgeInsets.all(15.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  color: accentColor1,
                  child: Text(
                    'PETA',
                    style: TextStyle(
                      color: Colors.black,
                      letterSpacing: 1.5,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'OpenSans',
                    ),
                  ),
                ),
                RaisedButton(
                  elevation: 5.0,
                  onPressed: () async {
                    context.bloc<TokenBloc>().add(RemoveToken());
                    context.bloc<TokenBloc>().add(GetToken());
                    context.bloc<PageBloc>().add(GoToLoginPage());
                  }, 
                  padding: EdgeInsets.all(15.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  color: Color(0xFFFAFAFA),
                  child: Text(
                    'LOGOUT',
                    style: TextStyle(
                      color: Colors.black,
                      letterSpacing: 1.5,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'OpenSans',
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ),
    );
  }
}