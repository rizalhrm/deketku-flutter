part of 'pages.dart';

class ForgotPassword extends StatefulWidget {
  final ForgotPasswordData forgotPasswordData;

  ForgotPassword(this.forgotPasswordData);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController usernameController = TextEditingController();
  bool isCheckingUsername = false;
  bool isUsernameValid = false;
  String _token;
  int _expires;

  _sendCodeToEmail(String yourEmail, String alias) {
    String code = getRandomString(5);
    String email = yourEmail;
    String fullname = alias;
    
    EmailVerificationServices.sendCode(fullname, yourEmail, code).then((_) {
      widget.forgotPasswordData.email = email;
      widget.forgotPasswordData.code = code;
      widget.forgotPasswordData.username = usernameController.text;
      widget.forgotPasswordData.tokenAdmin = _token;
      widget.forgotPasswordData.expires = _expires;

      setState(() {
        isCheckingUsername = false;
      });

      context.bloc<PageBloc>().add(GoToCodeVerificationPage(widget.forgotPasswordData));
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: !isCheckingUsername ? () async {
        context.bloc<PageBloc>().add(GoToLoginPage());
        return;
      } : () async {
        await AlertWarning.okayDialog(context, "Silahkan tunggu sebentar...", "Saat ini masih proses kirim kode verifikasi ke email kamu", false);
        return;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: !isCheckingUsername ? () { context.bloc<PageBloc>().add(GoToLoginPage()); } : () async {
                  await AlertWarning.okayDialog(context, "Silahkan tunggu sebentar...", "Saat ini masih proses kirim kode verifikasi ke email kamu", false);
                },
              );
            },
          ),
          title: Text(
            "Lupa Password",
            style: primaryFontLightBlack.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        body: BlocListener<UserBloc, UserState>(
          listener: (_, userState) async {
            if(userState is TokenLoadedAdmin) {
              if (userState.tokenAdmin.code == 200) {
                setState(() {
                  _token = userState.tokenAdmin.token;
                  _expires = userState.tokenAdmin.expires;
                });
                context.bloc<UserBloc>().add(GetUser(
                  usernameController.text, userState.tokenAdmin.token
                ));
              }
              else {
                setState(() {
                  isCheckingUsername = false;
                });
                Flushbar(
                  duration: Duration(seconds: 2),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Error. Silahkan coba lagi.",
                )..show(context);
              }
            }
            else if (userState is UserLoaded) {
              var count = userState.user.count;
              if (count == null) {
                if (userState.user.error.message == "Invalid Token") {
                  context.bloc<UserBloc>().add(ReloadUser(
                    usernameController.text, _token
                  ));
                } else {
                  setState(() {
                    isCheckingUsername = false;
                  });
                  Flushbar(
                    duration: Duration(seconds: 2),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Error. Silahkan coba lagi.",
                  )..show(context);
                }
              }
              else {
                if (count > 0) {
                  _sendCodeToEmail(userState.user.attributes.email, userState.user.attributes.alias);
                }
                else {
                  setState(() {
                    isCheckingUsername = false;
                  });
                  Flushbar(
                    duration: Duration(seconds: 3),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Username ${usernameController.text} tidak ditemukan.\nSilahkan periksa kembali username yang kamu masukkan dan coba lagi.",
                  )..show(context);
                }
              }
            }
            else if (userState is UserReloaded) {
              var count = userState.reloadUser.count;
              if (count == null) {
                setState(() {
                  isCheckingUsername = false;
                });
                Flushbar(
                  duration: Duration(seconds: 2),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Error. Silahkan coba lagi.",
                )..show(context);
              }
              else {
                if (count > 0) {
                  _sendCodeToEmail(userState.reloadUser.attributes.email, userState.reloadUser.attributes.alias);
                }
                else {
                  setState(() {
                    isCheckingUsername = false;
                  });
                  Flushbar(
                    duration: Duration(seconds: 3),
                    flushbarPosition: FlushbarPosition.TOP,
                    backgroundColor: Color(0xFFFF5C83),
                    message: "Username ${usernameController.text} tidak ditemukan.\nSilahkan periksa kembali username yang kamu masukkan dan coba lagi.",
                  )..show(context);
                }
              }
            }
            if(userState is ResponseError){
              setState(() {
                isCheckingUsername = false;
              });
              Flushbar(
                duration: Duration(seconds: userState.duration),
                flushbarPosition: FlushbarPosition.TOP,
                backgroundColor: Color(0xFFFF5C83),
                message: userState.message,
              )..show(context);
            }
          },
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              children: <Widget>[
                backgroundFrontPage,
                Container(
                  height: double.infinity,
                  width: double.infinity,
                  child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    padding:
                        EdgeInsets.fromLTRB(defaultMargin1, 120.0, defaultMargin1, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Masukkan Username Kamu',
                          style: primaryFontBoldBlack.copyWith(fontSize: 18.0),
                        ),
                        SizedBox(height: 25.0),
                        Container(
                          alignment: Alignment.centerLeft,
                          decoration: textBox,
                          height: 60.0,
                          child: TextField(
                            onChanged: (text) {
                              setState(() {
                                isUsernameValid = text.length >= 1;
                              });
                            },
                            controller: usernameController,
                            keyboardType: TextInputType.text,
                            style: secondaryFontRegularBlack,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(top: 14.0),
                              prefixIcon: Icon(
                                Icons.person,
                                color: Colors.black45,
                              ),
                              hintText: 'Username',
                              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45)
                            ),
                          ),
                        ),
                        SizedBox(height: 25.0),
                        isCheckingUsername 
                        ? SpinKitFadingCircle(
                            color: Color(0xFFFFFFFF),
                          )
                        : FloatingActionButton(
                            child: Icon(Icons.arrow_forward),
                            elevation: 2.0,
                            backgroundColor: isUsernameValid ? accentColor2 : Color(0xFFBEBEBE),
                            onPressed: isUsernameValid ? () async {
                              setState(() {
                                isCheckingUsername= true;
                              });

                              var statusDisconnected = await isDisconnected();
                              if (statusDisconnected) {
                                final action =
                                  await Dialogs.yesAbortDialog(
                                    context,
                                    titleAlertDisconnected,
                                    bodyAlertDisconnected,
                                    true
                                  );
                                if (action == DialogAction.yes) {
                                  AppSettings.openWIFISettings();
                                }
                                setState(() {
                                  isCheckingUsername = false;
                                });
                              } else {
                                context.bloc<UserBloc>().add(RefreshTokenAdmin(
                                  "rizal", "rizal1224", "admin", "user"
                                ));
                              }
                            } : null,
                          ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}