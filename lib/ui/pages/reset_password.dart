part of 'pages.dart';

class ResetPassword extends StatefulWidget {
  final ForgotPasswordData forgotPasswordData;

  ResetPassword(this.forgotPasswordData);

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  bool isPasswordValid = false;
  bool isConfirmPasswordValid = false;
  bool loading = false;
  bool passwordVisible = true;
  bool confirmPasswordVisible = true;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: !loading ? () async {
        context.bloc<PageBloc>().add(GoToCodeVerificationPage(widget.forgotPasswordData));
        return;
      } : () async {
        await AlertWarning.okayDialog(context, "Silahkan tunggu sebentar...", "Saat ini masih proses reset password", false);
        return;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: !loading ? () { context.bloc<PageBloc>().add(GoToCodeVerificationPage(widget.forgotPasswordData)); } : () async {
                  await AlertWarning.okayDialog(context, "Silahkan tunggu sebentar...", "Saat ini masih proses reset password", false);
                },
              );
            },
          ),
          title: Text(
            "Reset Password",
            style: primaryFontLightBlack.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        body: BlocListener<UserBloc, UserState>(
          listener: (_, userState) async {
            if(userState is TokenLoadedAdmin) {
              if (userState.tokenAdmin.code == 200) {
                context.bloc<UserBloc>().add(ResetPasswordEvent(
                  widget.forgotPasswordData.username, passwordController.text, userState.tokenAdmin.token
                ));
              }
              else {
                setState(() {
                  loading = false;
                });
                Flushbar(
                  duration: Duration(seconds: 2),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Error. Silahkan coba lagi.",
                )..show(context);
              }
            }
            else if (userState is ResetPasswordResult) {
              if (userState.result.contains('success')) {
                widget.forgotPasswordData.email = '';
                widget.forgotPasswordData.code = '';
                widget.forgotPasswordData.username = '';
                widget.forgotPasswordData.tokenAdmin = '';
                widget.forgotPasswordData.expires = 0;

                setState(() {
                  loading = false;
                });

                Flushbar(
                  duration: Duration(seconds: 4),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Colors.green[200],
                  message: "Ganti Password Berhasil.\nSilahkan kamu login dengan password baru!",
                )..show(context);

                context.bloc<PageBloc>().add(GoToLoginPage());
              } else {
                setState(() {
                  loading = false;
                });
                Flushbar(
                  duration: Duration(seconds: 2),
                  flushbarPosition: FlushbarPosition.TOP,
                  backgroundColor: Color(0xFFFF5C83),
                  message: "Error. Silahkan coba lagi.",
                )..show(context);
              }
            }
            if(userState is ResponseError){
              setState(() {
                loading = false;
              });
              Flushbar(
                duration: Duration(seconds: userState.duration),
                flushbarPosition: FlushbarPosition.TOP,
                backgroundColor: Color(0xFFFF5C83),
                message: userState.message,
              )..show(context);
            }
          },
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              children: <Widget>[
                backgroundFrontPage,
                Container(
                  height: double.infinity,
                  width: double.infinity,
                  child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    padding:
                        EdgeInsets.fromLTRB(defaultMargin1, 120.0, defaultMargin1, 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Buat Password baru (minimal 5 karakter)',
                          style: primaryFontBoldBlack.copyWith(fontSize: 18.0),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 20.0),
                        Container(
                          alignment: Alignment.centerLeft,
                          decoration: textBox,
                          height: 60.0,
                          child: TextField(
                            onChanged: (text) {
                              setState(() {
                                isPasswordValid = text.length >= 5;
                              });
                            },
                            controller: passwordController,
                            obscureText: passwordVisible,
                            style: secondaryFontRegularBlack,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(top: 14.0),
                              prefixIcon: Icon(
                                Icons.lock,
                                color: Colors.black45,
                              ),
                              hintText: 'Password Baru',
                              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45),
                              suffixIcon: IconButton(
                                icon: Icon(
                                  passwordVisible
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                                  color: Colors.black45,
                                ),
                                onPressed: () {
                                  setState(() {
                                    passwordVisible = !passwordVisible;
                                  });
                                }
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20.0),
                        Container(
                          alignment: Alignment.centerLeft,
                          decoration: textBox,
                          height: 60.0,
                          child: TextField(
                            onChanged: (text) {
                              setState(() {
                                isConfirmPasswordValid = text.length >= 5;
                              });
                            },
                            controller: confirmPasswordController,
                            obscureText: confirmPasswordVisible,
                            style: secondaryFontRegularBlack,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(top: 14.0),
                              prefixIcon: Icon(
                                Icons.lock,
                                color: Colors.black45,
                              ),
                              hintText: 'Konfirmasi Password',
                              hintStyle: secondaryFontRegularBlack.copyWith(color: Colors.black45),
                              suffixIcon: IconButton(
                                icon: Icon(
                                  confirmPasswordVisible
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                                  color: Colors.black45,
                                ),
                                onPressed: () {
                                  setState(() {
                                    confirmPasswordVisible = !confirmPasswordVisible;
                                  });
                                }
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20.0),
                        loading 
                        ? SpinKitFadingCircle(
                            color: Color(0xFFFFFFFF),
                          )
                        : RaisedButton(
                            elevation: 5.0,
                            onPressed: isPasswordValid && isConfirmPasswordValid ? () async {
                              setState(() {
                                loading = true;
                              });

                              var statusDisconnected = await isDisconnected();
                              if (statusDisconnected) {
                                final action =
                                  await Dialogs.yesAbortDialog(
                                    context,
                                    titleAlertDisconnected,
                                    bodyAlertDisconnected,
                                    true
                                  );
                                if (action == DialogAction.yes) {
                                  AppSettings.openWIFISettings();
                                }
                                setState(() {
                                  loading = false;
                                });
                              } else {
                                if (passwordController.text == confirmPasswordController.text) {
                                  context.bloc<UserBloc>().add(RefreshTokenAdmin(
                                    "rizal", "rizal1224", "admin", "admin"
                                  ));
                                } else {
                                  setState(() {
                                    loading = false;
                                  });
                                  Flushbar(
                                    duration: Duration(seconds: 2),
                                    flushbarPosition: FlushbarPosition.TOP,
                                    backgroundColor: Color(0xFFFF5C83),
                                    message: "Password baru dan konfirmasi password tidak sama.",
                                  )..show(context);
                                }
                              }
                            } : null,
                            padding: EdgeInsets.all(15.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            color: isPasswordValid && isConfirmPasswordValid
                                ? Color(0xFFFAFAFA)
                                : Color(0xFFBEBEBE),
                            child: Text(
                              'SIMPAN',
                              style: primaryFontBoldBlack.copyWith(
                                color: isPasswordValid && isConfirmPasswordValid
                                    ? Colors.black
                                    : Color(0xFFBEBEBE),
                                letterSpacing: 1.5,
                                fontSize: 18.0,
                              )
                            ),
                          ),
                      ]
                    )
                  )
                )
              ]
            ),
          )
        )
      )
    );
  }
}