part of 'widgets.dart';

class MapPopup extends StatefulWidget {
  //final Marker marker;
  final String title;
  //final bool isDirection;

  MapPopup(this.title, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MapPopupState(this.title);
}

class _MapPopupState extends State<MapPopup> {
  //final Marker _marker;
  final String title;
  //final bool isDirection;

  _MapPopupState(this.title);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _cardDescription(context),
          // isDirection ?
          //   InkWell(
          //     child: Padding(
          //       padding: EdgeInsets.only(left: 10, right: 10),
          //       child: Icon(Icons.star),
          //     ),
          //     onTap: () => print('direction'),
          //   )
          // : SizedBox(height: 0,)
        ],
      ),
    );
  }

  Widget _cardDescription(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        constraints: BoxConstraints(minWidth: 100, maxWidth: 200),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              title,
              overflow: TextOverflow.fade,
              softWrap: false,
              style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14.0,
              ),
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 4.0)),
            // Text(
            //   "Position: ${_marker.point.latitude}, ${_marker.point.longitude}",
            //   style: const TextStyle(fontSize: 12.0),
            // ),
            // Text(
            //   "Marker size: ${_marker.width}, ${_marker.height}",
            //   style: const TextStyle(fontSize: 12.0),
            // ),
          ],
        ),
      ),
    );
  }
}
