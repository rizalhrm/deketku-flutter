part of 'widgets.dart';

final textBox = BoxDecoration(
  color: Color(0xFFFAFAFA),
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2)
    )
  ]
);

final backgroundFrontPage = Container(
  height: double.infinity,
  width: double.infinity,
  decoration: BoxDecoration(
    image: DecorationImage(
      image: AssetImage('assets/images/bg.jpg'),
      fit: BoxFit.cover,
    )
  ),
);