part of 'widgets.dart';

class ActionSheet {
  static cupertino(
    BuildContext context,
    String title,
    Function options1,
    Function options2,
    String textOptions1,
    String textOptions2,
  ) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) {
        return CupertinoActionSheet(
          title: Text(title),
          cancelButton: CupertinoActionSheetAction(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Batal')
          ),
          actions: <Widget>[
            CupertinoActionSheetAction(
              onPressed: () {options1();},
              child: Text(textOptions1)
            ),
            CupertinoActionSheetAction(
              onPressed: () {options2();},
              child: Text(textOptions2)
            ),
          ],
        );
      }
    );
  }
}