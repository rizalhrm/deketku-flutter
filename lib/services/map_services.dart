part of 'services.dart';

class MapServices {
  static Future<String> analysisPolygon(String analysisType, String analysisValue, String unit, String geometryParams, {http.Client client}) async {
    String url;

    if (analysisType == "Buffer") {
      url = "http://tig.co.id/ags/rest/services/GP/v2_OriProjArea/GPServer/OriProjArea/execute?Distances=$analysisValue&Buffer_Unit=$unit&Input_Features=$geometryParams&f=json&env:outSR=4326&env:processSR=4326";

      // response = await http.post(url, body: {
      //   "Distances": widget.options.analysisValue,
      //   "Buffer_Unit": widget.options.unit,
      //   "Input_Features": widget.options.geometryParams,
      //   "f": "json",
      //   "env:outSR": "4326",
      //   "env:processSR": "4326"
      // });
    } else if (analysisType == "Driving") {
      url = "http://tig.co.id/ags/rest/services/GP/DriveTime32223232/GPServer/DriveTime3/execute?B_Values=$analysisValue&Break_Units=$unit&Facilities=$geometryParams&f=json&env:outSR=4326&env:processSR=4326";

      // response = await http.post(url, body: {
      //   "B_Values": widget.options.analysisValue,
      //   "Break_Units": widget.options.unit,
      //   "Facilities": widget.options.geometryParams,
      //   "f": "json",
      //   "env:outSR": "4326",
      //   "env:processSR": "4326"
      // });
    }

    var response = await http.get(url);
    var jsonData = json.decode(response.body);
    String result;
    if (jsonData["results"] != null) {
      for (var feature in jsonData["results"][0]["value"]["features"]) {
        result=json.encode(feature["geometry"]["rings"]);
      }
    }
    return result;
  }
}