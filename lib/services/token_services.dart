part of 'services.dart';

class TokenServices {
  static Future<Token> generateToken(String username, String password, String role, String web, {http.Client client}) async {
    String url = (web == "user") ? "https://gis.locatorlogic.com/arcgis/tokens/generateToken" : "https://gis.locatorlogic.com/arcgis/admin/generateToken";

    String hashPassword = (role == "user") ? encryptPassword(password) : password;

    client ??= http.Client();

    var response = await client.post(url, body: {
      "username": username.replaceAll("'", "''"),
      "password": hashPassword,
      "client": "requestip",
      "expiration": "1440",
      "f": "json"
    });

    var data = json.decode(response.body);
    
    if (web == "user") {
      Token tokenModel = Token.convertUser(data);
      return tokenModel;
    }
    else {
      Token tokenModel = Token.convertAdmin(data);
      return tokenModel;
    }
  }
}