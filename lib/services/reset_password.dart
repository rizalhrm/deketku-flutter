part of 'services.dart';

class ResetPasswordServices {
  static Future reset(String username, String password, String token, {http.Client client}) async {
    String url = 'https://gis.locatorlogic.com/arcgis/admin/security/users/update?token=$token';

    client ??= http.Client();

    String hashPassword = encryptPassword(password);

    var response = await client.post(url, body: {
      "username": username.replaceAll("'", "''"),
      "password": hashPassword,
      "f": "json"
    });

    return response.body;
  }
}