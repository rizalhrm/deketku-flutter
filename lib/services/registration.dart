part of 'services.dart';

class RegistrationServices{
  static Future<String> uploadPhotoProfile(String token, File photo, int objectId) async {
    Uri apiUrl = Uri.parse('$deketkuServicesUrl/16/${objectId.toString()}/addAttachment?token=$token');
    final mimeTypeData =
        lookupMimeType(photo.path, headerBytes: [0xFF, 0xD8]).split('/');
    
    // Intilize the multipart request
    final imageUploadRequest = http.MultipartRequest('POST', apiUrl);

    // Attach the file in the request
    final file = await http.MultipartFile.fromPath(
        'attachment', photo.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));

    imageUploadRequest.files.add(file);
    imageUploadRequest.fields['f'] = "json";

    try {
      final streamedResponse = await imageUploadRequest.send();
      final response = await http.Response.fromStream(streamedResponse);
      return (response.body);
    } catch (e) {
      return e;
    }
  }

  static Future<String> addUserToArcGISServer(String token, String password, String username, String fullname, String email, {http.Client client}) async {
    String url = 'https://gis.locatorlogic.com/arcgis/admin/security/users/add?token=$token';

    client ??= http.Client();

    String hashPassword = encryptPassword(password);

    var response = await client.post(url, body: {
      "username": username,
      "password": hashPassword,
      "fullname": fullname,
      "description": "user deketku registrasi dengan manual (deketku flutter)",
      "email": email,
      "f": "json"
    });

    return response.body;
  }

  static Future<String> addUsersToRole(String token, String username,{http.Client client}) async {
    String url = 'https://gis.locatorlogic.com/arcgis/admin/security/roles/addUsersToRole?token=$token';

    client ??= http.Client();

    var response = await client.post(url, body: {
      "rolename": "deketku",
      "users": username,
      "csrfPreventToken": "UHhBTnwUoTRliq0eDE41NhXDaHyNrQAK4n6bcFEjq1lkyrMVOdEFOe4JMXdgUeVk,UHhBTnwUoTRliq0eDE41NukTGfnjdmIUmgfs2KnAMQkWojC0Iupvb5lp2iMRi4JzT0WXT3g5BK4sRs1GDflJLg_WJMCEjZwLiy20Y66PleE.",
      "f": "json"
    });

    return response.body;
  }

  static Future<AddNewUser> addFeatures(String token, String feature, int idServices, {http.Client client}) async {
    String url = "$deketkuServicesUrl/$idServices/addFeatures";

    client ??= http.Client();

    var response = await client.post(url, body: {
      "gdbVersion": "",
      "features": feature,
      "f": "json"
    }, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});

    var data = json.decode(response.body);
    AddNewUser result = AddNewUser.fromJson(data, response.body);
    return result;
  }

  static Future<String> removeUserFromArcGISServer(String token, String username, {http.Client client}) async {
    String url = 'https://gis.locatorlogic.com/arcgis/admin/security/users/remove?token=$token';

    client ??= http.Client();

    var response = await client.post(url, body: {
      "username": username,
      "csrfPreventToken": "kVww3fvwrjRuOWpMcMzQBm7UEeTzCqfYIVf6tAv05fqLjphdAjOTmau4dP1Skf1m,kVww3fvwrjRuOWpMcMzQBhaL7H4CH400kL7K_LAoOGx2M57bhZdiYFONeQqYyX6_siN8icphWw3E-QrXmGV-3AFkhtZyC6Bi4cagPLOzwjM.",
      "f": "json"
    });
    
    return response.body;
  }
}