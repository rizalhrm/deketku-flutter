part of 'services.dart';

class EmailVerificationServices {
  static Future sendCode(String fullname, String email, String code, {http.Client client}) async {
    String url = 'https://email-verification.deketku.com/action.php';

    client ??= http.Client();

    await client.post(url, body: {
      "name": fullname,
      "email": email,
      "code": code
    });

    return;
  }
}