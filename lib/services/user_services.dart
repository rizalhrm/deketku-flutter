part of 'services.dart';

class UserServices {
  static Future<User> getUser(String username, String token, String outFields, {http.Client client}) async {

    client ??= http.Client();
    String newUsername = username.replaceAll("'", "''");
    String url = "$deketkuServicesUrl/16/query?token=$token&f=json&where=username='$newUsername'&outFields=$outFields";

    var response = await client.get(url);
    // var response = await client.post(url, body: {
    //   "where": "username='$newUsername'",
    //   "outFields": outFields,
    //   "f": "json"
    // }, headers: {HttpHeaders.authorizationHeader: "Bearer $token"},);

    var data = json.decode(response.body);

    User userModel = User.fromJson(data);
    return userModel;
  }

  static Future<UserCount> getUserCount(String username, String token, {http.Client client}) async {
    String newUsername = username.replaceAll("'", "''");
    String url = "$deketkuServicesUrl/16/query?token=$token&f=json&where=username='$newUsername'&returnCountOnly=true";
    client ??= http.Client();
    
    var response = await client.get(url);

    // var response = await client.post(url, body: {
    //   "where": "username='$newUsername'",
    //   "returnCountOnly": "true",
    //   "f": "json"
    // }, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
    
    var data = json.decode(response.body);
    UserCount result = UserCount.fromJson(data);
    return result;
  }
}