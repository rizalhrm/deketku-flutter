import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'package:deketku/models/models.dart';
import 'package:deketku/shared/shared.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';

part 'token_services.dart';
part 'user_services.dart';
part 'email_verification.dart';
part 'reset_password.dart';
part 'registration.dart';
part 'map_services.dart';