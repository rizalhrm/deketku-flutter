part of 'page_bloc.dart';

abstract class PageEvent extends Equatable {
  const PageEvent();
}

class GoToLoginPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToMainPage extends PageEvent {
  final CoordinatesInitial coords;

  GoToMainPage(this.coords);

  @override
  List<Object> get props => [coords];
}

class GoToForgotPasswordPage extends PageEvent {
  final ForgotPasswordData forgotPasswordData;

  GoToForgotPasswordPage(this.forgotPasswordData);

  @override
  List<Object> get props => [];
}

class GoToCodeVerificationPage extends PageEvent {
  final ForgotPasswordData forgotPasswordData;

  GoToCodeVerificationPage(this.forgotPasswordData);
  
  @override
  List<Object> get props => [];
}

class GoToResetPasswordPage extends PageEvent {
  final ForgotPasswordData forgotPasswordData;

  GoToResetPasswordPage(this.forgotPasswordData);
  
  @override
  List<Object> get props => [];
}

class GoToRegistrationPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToMapPage extends PageEvent {
  final CoordinatesInitial coords;

  GoToMapPage(this.coords);

  @override
  List<Object> get props => [coords];
}