import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:deketku/models/models.dart';

part 'page_event.dart';
part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  @override
  PageState get initialState => OnInitialPage();

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if (event is GoToLoginPage) {
      yield OnLoginPage();
    } else if (event is GoToMainPage) {
      yield OnMainPage(event.coords);
    } else if (event is GoToForgotPasswordPage) {
      yield OnForgotPasswordPage(event.forgotPasswordData);
    } else if (event is GoToCodeVerificationPage) {
      yield OnCodeVerificationPage(event.forgotPasswordData);
    } else if (event is GoToResetPasswordPage) {
      yield OnResetPasswordPage(event.forgotPasswordData);
    } else if (event is GoToRegistrationPage) {
      yield OnRegistrationPage();
    } else if (event is GoToMapPage) {
      yield OnMapPage(event.coords);
    }
  }
}
