part of 'page_bloc.dart';

abstract class PageState extends Equatable {
  const PageState();
}

class OnInitialPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnLoginPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnMainPage extends PageState {
  final CoordinatesInitial coords;

  OnMainPage(this.coords);

  @override
  List<Object> get props => [coords];
}

class OnForgotPasswordPage extends PageState {
  final ForgotPasswordData forgotPasswordData;

  OnForgotPasswordPage(this.forgotPasswordData);
  
  @override
  List<Object> get props => [];
}

class OnCodeVerificationPage extends PageState {
  final ForgotPasswordData forgotPasswordData;

  OnCodeVerificationPage(this.forgotPasswordData);
  
  @override
  List<Object> get props => [];
}

class OnResetPasswordPage extends PageState {
  final ForgotPasswordData forgotPasswordData;

  OnResetPasswordPage(this.forgotPasswordData);
  
  @override
  List<Object> get props => [];
}

class OnRegistrationPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnMapPage extends PageState {
  final CoordinatesInitial coords;

  OnMapPage(this.coords);

  @override
  List<Object> get props => [coords];
}