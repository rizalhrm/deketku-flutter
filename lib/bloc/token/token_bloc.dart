import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:deketku/models/models.dart';

part 'token_event.dart';
part 'token_state.dart';

class TokenBloc extends Bloc<TokenEvent, TokenState> {
  @override
  TokenState get initialState => TokenInitial();

  @override
  Stream<TokenState> mapEventToState(
    TokenEvent event,
  ) async* {
    if (event is GetToken) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String checkUsername = prefs.getString('username');
      String checkPassword = prefs.getString('password');
      String token = prefs.getString('token');
      int expires = prefs.getInt('expires');

      Token tokenUser = Token(username: checkUsername, password: checkPassword, token: token, expires: expires);

      yield TokenLoadedUser(tokenUser);
    }
    else if (event is RemoveToken) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove("username");
      prefs.remove("password");
      prefs.remove("token");
      prefs.remove("expires");
      
      yield TokenInitial();
    }
  }
}
