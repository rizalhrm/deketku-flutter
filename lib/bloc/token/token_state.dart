part of 'token_bloc.dart';

abstract class TokenState extends Equatable {
  const TokenState();
}

class TokenInitial extends TokenState {
  @override
  List<Object> get props => [];
}

class TokenLoadedUser extends TokenState {
  final Token tokenUser;

  TokenLoadedUser(this.tokenUser);

  @override
  List<Object> get props => [tokenUser];
}