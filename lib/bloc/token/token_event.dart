part of 'token_bloc.dart';

abstract class TokenEvent extends Equatable {
  const TokenEvent();
}

class GetToken extends TokenEvent {
  @override
  List<Object> get props => [];
}

class RemoveToken extends TokenEvent {
  @override
  List<Object> get props => [];
}

class RefreshTokenUser extends TokenEvent {
  final String username;
  final String password;
  final String role;
  final String web;
  
  RefreshTokenUser(this.username, this.password, this.role, this.web);

  @override
  List<Object> get props => [username, password, role, web];
}