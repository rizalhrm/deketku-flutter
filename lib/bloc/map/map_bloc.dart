import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_map/flutter_map.dart';

import 'package:deketku/models/models.dart';
import 'package:deketku/services/services.dart';

part 'map_event.dart';
part 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
 @override
  MapState get initialState => MapInitial();

  @override
  Stream<MapState> mapEventToState(
    MapEvent event,
  ) async* {
    if (event is MapWaiting) {
      yield MapLoaded();
    }
    else if (event is AddMarker) {
      yield* _addMarker(event.attributes, event.marker);
    }
    else if (event is UpdateMarker) {
      yield* _updateMarker(event.attributes, event.marker);
    }
    else if (event is CreateAnalysisPolygon) {
      yield* _createAnalysisPolygon(event.analysisType,event.analysisValue,event.unit,event.geometryParams);
    }
  }

  Stream<MapState> _addMarker(attributes, marker) async*{
    try {
      Poi poi = Poi(attributes, marker);
      List<Poi> poiList = [];
      poiList.add(poi);
      yield MarkerLoaded(poiList); 
    } catch (e) {
      yield MapResponseError(e.toString(), 3);
    }
  }


  Stream<MapState> _updateMarker(attributes, marker) async*{
    try {
      Poi poi = Poi(attributes, marker);
      List<Poi> poiList = (state as MarkerLoaded).poi;
      for (var marker in poiList) {
        if (marker.attributes != poi.attributes) {
          poiList.add(poi);
        }
      }
      yield MarkerLoaded(poiList); 
    } catch (e) {
      yield MapResponseError(e.toString(), 3);
    }
  }

  Stream<MapState> _createAnalysisPolygon(analysisType,analysisValue,unit,geometryParams) async*{
    try {
      String analysis = await MapServices.analysisPolygon(analysisType,analysisValue,unit,geometryParams);
      yield PolygonState(analysis); 
    } catch (e) {
      yield MapResponseError(e.toString(), 3);
    }
  }
}
