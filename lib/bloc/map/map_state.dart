part of 'map_bloc.dart';

abstract class MapState extends Equatable {
  const MapState();
}

class MapInitial extends MapState {
  @override
  List<Object> get props => [];
}

class MapLoaded extends MapState {
  @override
  List<Object> get props => [];
}

class MarkerLoaded extends MapState {
  final List<Poi> poi;

  MarkerLoaded(this.poi);

  @override
  List<Object> get props => [poi];
}

class PolygonState extends MapState {
  final String rings;

  PolygonState(this.rings);

  @override
  List<Object> get props => [rings];
}

class MapResponseError extends MapState {
  final String message;
  final int duration;

  MapResponseError(this.message, this.duration);

  @override
  List<Object> get props => [message, duration];
}