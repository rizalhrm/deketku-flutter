part of 'map_bloc.dart';

abstract class MapEvent extends Equatable {
  const MapEvent();
}

class MapWaiting extends MapEvent {
  @override
  List<Object> get props => [];
}

class CreateAnalysisPolygon extends MapEvent {
  final String analysisType;
  final String analysisValue;
  final String unit;
  final String geometryParams;

  CreateAnalysisPolygon(this.analysisType,this.analysisValue,this.unit,this.geometryParams);

  @override
  List<Object> get props => [analysisType,analysisValue,unit,geometryParams];
}

class AddMarker extends MapEvent {
  final String attributes;
  final Marker marker;

  AddMarker(this.attributes, this.marker);

  @override
  List<Object> get props => [attributes, marker];
}

class UpdateMarker extends MapEvent {
  final String attributes;
  final Marker marker;

  UpdateMarker(this.attributes, this.marker);

  @override
  List<Object> get props => [attributes, marker];
}