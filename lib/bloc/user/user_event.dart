part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();
}

class RefreshTokenAdmin extends UserEvent {
  final String username;
  final String password;
  final String role;
  final String web;
  
  RefreshTokenAdmin(this.username, this.password, this.role, this.web);

  @override
  List<Object> get props => [username, password, role, web];
}

class GetUserCount extends UserEvent {
  final String username;
  final String tokenAdmin;

  GetUserCount(this.username, this.tokenAdmin);

  @override
  List<Object> get props => [username, tokenAdmin];
}

class ReloadUserCount extends UserEvent {
  final String username;
  final String tokenAdmin;

  ReloadUserCount(this.username, this.tokenAdmin);

  @override
  List<Object> get props => [username, tokenAdmin];
}

class LoginUser extends UserEvent {
  final String username;
  final String password;
  final String role;
  final String web;
  
  LoginUser(this.username, this.password, this.role, this.web);

  @override
  List<Object> get props => [username, password, role, web];
}

class GetUser extends UserEvent {
  final String username;
  final String tokenAdmin;

  GetUser(this.username, this.tokenAdmin);

  @override
  List<Object> get props => [username, tokenAdmin];
}

class ReloadUser extends UserEvent {
  final String username;
  final String tokenAdmin;

  ReloadUser(this.username, this.tokenAdmin);

  @override
  List<Object> get props => [username, tokenAdmin];
}

class ResetPasswordEvent extends UserEvent {
  final String username;
  final String password;
  final String tokenAdmin;

  ResetPasswordEvent(this.username, this.password, this.tokenAdmin);

  @override
  List<Object> get props => [username, password, tokenAdmin];
}

class AddUserToArcGISServer extends UserEvent {
  final String tokenAdmin;
  final String password;
  final String username;
  final String fullname;
  final String email;

  AddUserToArcGISServer(this.tokenAdmin, this.password, this.username, this.fullname, this.email);

  @override
  List<Object> get props => [tokenAdmin, password, username, fullname, email];
}

class AddUsersToRole extends UserEvent {
  final String tokenAdmin;
  final String username;

  AddUsersToRole(this.tokenAdmin, this.username);

  @override
  List<Object> get props => [tokenAdmin, username];
}

class AddUserToFeaturesServices extends UserEvent {
  final String tokenAdmin;
  final String featureForAddUser;
  final int idServices;

  AddUserToFeaturesServices(this.tokenAdmin, this.featureForAddUser, this.idServices);

  @override
  List<Object> get props => [tokenAdmin, featureForAddUser, idServices];
} 

class UploadPhotoProfile extends UserEvent {
  final String tokenAdmin;
  final File photo;
  final int objectId;

  UploadPhotoProfile(this.tokenAdmin, this.photo, this.objectId);

  @override
  List<Object> get props => [tokenAdmin, photo, objectId];
}