import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:deketku/models/models.dart';
import 'package:deketku/services/services.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  @override
  UserState get initialState => UserInitial();

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is RefreshTokenAdmin) {
      try {
        Token tokenUser = await TokenServices.generateToken(event.username, event.password, event.role, event.web);
        yield TokenLoadedAdmin(tokenUser);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is GetUserCount) {
      try {
        UserCount count = await UserServices.getUserCount(event.username, event.tokenAdmin);
        yield CountLoaded(count);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is LoginUser) {
      try {
        Token login = await TokenServices.generateToken(event.username, event.password, event.role, event.web);
        yield LoginSuccess(login);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is ReloadUserCount) {
      try {
        UserCount count = await UserServices.getUserCount(event.username, event.tokenAdmin);
        yield CountReloaded(count);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is GetUser) {
      try {
        User user = await UserServices.getUser(event.username, event.tokenAdmin, "*");
        yield UserLoaded(user);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is ReloadUser) {
      try {
        User user = await UserServices.getUser(event.username, event.tokenAdmin, "*");
        yield UserReloaded(user);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is ResetPasswordEvent) {
      try {
        String result = await ResetPasswordServices.reset(event.username, event.password, event.tokenAdmin);
        yield ResetPasswordResult(result);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is AddUserToArcGISServer) {
      try {
        String result = await RegistrationServices.addUserToArcGISServer(event.tokenAdmin, event.password, event.username, event.fullname, event.email);
        yield ResultAddUserToArcGISServer(result);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is AddUsersToRole) {
      try {
        String result = await RegistrationServices.addUsersToRole(event.tokenAdmin, event.username);
        yield ResultAddUsersToRole(result);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is AddUserToFeaturesServices) {
      try {
        AddNewUser results = await RegistrationServices.addFeatures(event.tokenAdmin, event.featureForAddUser, event.idServices);
        yield ResultsAddUserToFeaturesServices(results);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
    else if (event is UploadPhotoProfile) {
      try {
        String result = await RegistrationServices.uploadPhotoProfile(event.tokenAdmin, event.photo, event.objectId);
        yield ResultUploadPhotoProfile(result);
      } catch (e) {
        yield ResponseError(e.toString(), 3);
      }
    }
  }
}
