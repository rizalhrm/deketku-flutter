part of 'user_bloc.dart';

abstract class UserState extends Equatable {
  const UserState();
}

class UserInitial extends UserState {
  @override
  List<Object> get props => [];
}

class LoginInitial extends UserState {
  @override
  List<Object> get props => [];
}

class TokenLoadedAdmin extends UserState {
  final Token tokenAdmin;

  TokenLoadedAdmin(this.tokenAdmin);

  @override
  List<Object> get props => [tokenAdmin];
}

class ResponseError extends UserState {
  final String message;
  final int duration;

  ResponseError(this.message, this.duration);

  @override
  List<Object> get props => [message, duration];
}

class CountLoaded extends UserState {
  final UserCount count;

  CountLoaded(this.count);

  @override
  List<Object> get props => [count];
}

class LoginSuccess extends UserState {
  final Token login;

  LoginSuccess(this.login);

  @override
  List<Object> get props => [login];
}

class CountReloaded extends UserState {
  final UserCount recount;

  CountReloaded(this.recount);

  @override
  List<Object> get props => [recount];
}

class UserLoaded extends UserState {
  final User user;

  UserLoaded(this.user);

  @override
  List<Object> get props => [user];
}

class UserReloaded extends UserState {
  final User reloadUser;

  UserReloaded(this.reloadUser);

  @override
  List<Object> get props => [reloadUser];
}

class ResetPasswordResult extends UserState {
  final String result;

  ResetPasswordResult(this.result);

  @override
  List<Object> get props => [result];
}

class ResultAddUserToArcGISServer extends UserState {
  final String resultAddUser;

  ResultAddUserToArcGISServer(this.resultAddUser);

  @override
  List<Object> get props => [resultAddUser];
}

class ResultAddUsersToRole extends UserState {
  final String resultAddRole;

  ResultAddUsersToRole(this.resultAddRole);

  @override
  List<Object> get props => [resultAddRole];
}

class ResultsAddUserToFeaturesServices extends UserState {
  final AddNewUser addUserModel;

  ResultsAddUserToFeaturesServices(this.addUserModel);

  @override
  List<Object> get props => [addUserModel];
}

class ResultUploadPhotoProfile extends UserState {
  final String resultUpload;

  ResultUploadPhotoProfile(this.resultUpload);

  @override
  List<Object> get props => [resultUpload];
}